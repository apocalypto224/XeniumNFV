package com.ahghorab.xenonet.service;

import com.ahghorab.xenonet.repository.OveralDiagramConnectionRepository;
import com.ahghorab.xenonet.repository.ServerRepository;
import com.ahghorab.xenonet.security.SecurityUtils;
import com.ahghorab.xenonet.service.util.event.EventTask;
import com.ahghorab.xenonet.web.rest.vm.EventDiagramVM;
import com.ahghorab.xenonet.web.rest.vm.ServerVM;
import com.ahghorab.xenonet.web.rest.vm.event.EventVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class EventService {

    private final Logger log = LoggerFactory.getLogger(EventService.class);

    private final UserService userService;
    private final NetworkDiagramService networkDiagramService;
    private List<EventVM> eventVMList = new ArrayList<>();
    private Map<Long, EventVM> eventIdMap = new HashMap<>();
    private Map<Long, EventTask> eventTaskIdMap = new HashMap<>();

    private final OveralDiagramConnectionRepository overalDiagramConnectionRepository;

    public EventService(ServerRepository serverRepository, UserService userService, NetworkDiagramService networkDiagramService, OveralDiagramConnectionRepository overalDiagramConnectionRepository) {
        this.userService = userService;
        this.networkDiagramService = networkDiagramService;
        this.overalDiagramConnectionRepository = overalDiagramConnectionRepository;
    }

    public Integer createEvent(EventVM event) {
        eventIdMap.put(this.eventVMList.size() + 1L, event);
        event.setId(this.eventVMList.size() + 1L);
        this.eventVMList.add(event);
        return 1;
    }

    public Integer startEvent(Long eventId) {
        /*
        Start Event task with kafka startingOffset = -1 ==> seekToEnd()
         */
        EventTask eventTask = new EventTask(eventIdMap.get(eventId), this.networkDiagramService, -1);
        EventVM evmTemp = eventIdMap.get(eventId);
        evmTemp.setStatus("up");
        eventIdMap.put(eventId, evmTemp);
        eventTask.start();
        eventTaskIdMap.put(eventId, eventTask);
        return null;
    }

    public Integer stopEvent(Long eventId) {
        /*
        find event by Id in future get it from DB
         */
        EventTask eventTask = eventTaskIdMap.get(eventId);
        EventVM evmTemp = eventIdMap.get(eventId);
        evmTemp.setStatus("down");
        eventIdMap.put(eventId, evmTemp);
        eventTask.stop();
        eventTaskIdMap.put(eventId, eventTask);
        return null;
    }

    //    @Transactional(readOnly = true)
    public Page<EventVM> getAllEvents(Pageable pageable) {
        if(eventVMList.size() > 0) {
            eventVMList.forEach(evnt -> {
                if(eventTaskIdMap.get(evnt.getId()) != null) {
                    evnt.setNumberOfOccur(String.valueOf(eventTaskIdMap.get(evnt.getId()).getNumberOfOccure()));
                }
            });
        }
        return new PageImpl<EventVM>(eventVMList, pageable, eventVMList.size());
//        return serverRepository.findBy(SecurityUtils.getCurrentUserLogin(), pageable).map(ServerVM::new);
    }


}
