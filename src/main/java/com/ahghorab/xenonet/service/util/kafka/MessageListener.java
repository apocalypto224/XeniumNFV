package com.ahghorab.xenonet.service.util.kafka;

import org.springframework.kafka.annotation.KafkaListener;

public class MessageListener {

    @KafkaListener(topics = "${message.topic.name}", groupId = "${spring.kafka.consumer.group-id}", containerFactory = "fooKafkaListenerContainerFactory")
    public void listenGroupFoo(String message) {
        System.out.println("Received Messasge in group: " + message);
    }

}
