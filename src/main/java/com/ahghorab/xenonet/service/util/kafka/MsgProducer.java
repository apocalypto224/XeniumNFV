package com.ahghorab.xenonet.service.util.kafka;

import com.ahghorab.xenonet.service.util.monitoring.MonitoredContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;

public class MsgProducer {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

//    @Autowired
//    private KafkaTemplate<String, MonitoredContainer> MonitoredContainerKafkaTemplate;

    public void sendMessage(String topic, String message) {
        kafkaTemplate.send(topic, message);
    }

//    public void sendMonitoredContainerMessage(MonitoredContainer monitoredContainer) {
//        MonitoredContainerKafkaTemplate.send("T_VNF", monitoredContainer);
//        }
}
