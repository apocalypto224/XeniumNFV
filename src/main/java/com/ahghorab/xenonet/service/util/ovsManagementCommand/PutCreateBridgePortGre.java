package com.ahghorab.xenonet.service.util.ovsManagementCommand;

import com.ahghorab.xenonet.service.util.httpReq.HttpRequestSender;
import com.ahghorab.xenonet.service.util.httpReq.HttpRequestTypeEnum;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class PutCreateBridgePortGre {
    private String nodeId;
    private String bridgeName;
    private String portName;
    private String portOptionKey;
    private String portOptionValue;
    private String portOption2Key;
    private String portOption2Value;
    private String interfaceType;

    public PutCreateBridgePortGre(String nodeId, String bridgeName, String portName, String portOptionKey, String portOptionValue, String portOption2Key, String portOption2Value, String interfaceType) {
        this.nodeId = nodeId;
        this.bridgeName = bridgeName;
        this.portName = portName;
        this.portOptionKey = portOptionKey;
        this.portOptionValue = portOptionValue;
        this.portOption2Key = portOption2Key;
        this.portOption2Value = portOption2Value;
        this.interfaceType = interfaceType;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getBridgeName() {
        return bridgeName;
    }

    public void setBridgeName(String bridgeName) {
        this.bridgeName = bridgeName;
    }

    public String getPortName() {
        return portName;
    }

    public void setPortName(String portName) {
        this.portName = portName;
    }

    public String getPortOptionKey() {
        return portOptionKey;
    }

    public void setPortOptionKey(String portOptionKey) {
        this.portOptionKey = portOptionKey;
    }

    public String getPortOptionValue() {
        return portOptionValue;
    }

    public void setPortOptionValue(String portOptionValue) {
        this.portOptionValue = portOptionValue;
    }

    public String getPortOption2Key() {
        return portOption2Key;
    }

    public void setPortOption2Key(String portOption2Key) {
        this.portOption2Key = portOption2Key;
    }

    public String getPortOption2Value() {
        return portOption2Value;
    }

    public void setPortOption2Value(String portOption2Value) {
        this.portOption2Value = portOption2Value;
    }

    public String getInterfaceType() {
        return interfaceType;
    }

    public void setInterfaceType(String interfaceType) {
        this.interfaceType = interfaceType;
    }

    public Integer execute() {
        HttpRequestSender reqSender;
        try {
            reqSender = new HttpRequestSender(
                this.getUri(),
                new StringEntity(this.getBody().toJSONString(), ContentType.APPLICATION_JSON),
                HttpRequestTypeEnum.PUT
            );
            try {
                reqSender.execute();
                return reqSender.getResStatus();

            } catch (Exception e) {
                System.out.println("Bad Request");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return 0;
    }

    /*
     {
  "network-topology:termination-point": [
    {
       "tp-id": "gre11",
        "ovsdb:interface-type": "ovsdb:interface-type-gre",
        "ovsdb:name": "gre11",
        "ovsdb:options": [
           {
           "option": "remote_ip",
           "value": "192.168.59.129"
           },
           {
           "option": "key",
           "value": "1"
           }
        ]
    }
  ]
}
         */
    public JSONObject getBody() {
        JSONObject jsonObj = new JSONObject();
        JSONObject port = new JSONObject();

        port.put("ovsdb:name", portName);
        port.put("tp-id", portName);
        port.put("ovsdb:interface-type", interfaceType);

        //Option Part
        if (portOptionKey != null) {
            JSONArray portOptions = new JSONArray();
            JSONObject optionObj = new JSONObject();
            optionObj.put("ovsdb:option", portOptionKey);
            optionObj.put("ovsdb:value", portOptionValue);
            portOptions.add(optionObj);
            if (portOption2Key != null) {
                // Option 2 for Gre Key
                JSONObject option2Obj = new JSONObject();
                option2Obj.put("ovsdb:option", portOption2Key);
                option2Obj.put("ovsdb:value", portOption2Value);
                portOptions.add(option2Obj);
            }
            port.put("ovsdb:options", portOptions);
        }

        jsonObj.put("network-topology:termination-point", port);
        return jsonObj;
    }

    public String getUri() {
        return "http://localhost:8181/restconf/config/network-topology:network-topology/topology/ovsdb:1/node/" + this.nodeId.split("//")[0] + "%2F%2F" + this.nodeId.split("//")[1] + "%2Fbridge%2F" + bridgeName + "/termination-point/" + portName + "/";
    }
}
