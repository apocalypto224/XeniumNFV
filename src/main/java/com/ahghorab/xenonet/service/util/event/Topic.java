package com.ahghorab.xenonet.service.util.event;

import com.ahghorab.xenonet.service.util.monitoring.MonitoredContainer;

public class Topic {
    private String name;
    private String networkEntityName;
    private String metric;
    /*
    Name of the network entity part that we monitored its metric in our case -> network interfaces
     */
    private String networkEntityInterfaceName;
    private String value;
    private MonitoredContainer monitoredContainer;

    public Topic(String name, String networkEntityName, String metric, String networkEntityInterfaceName, String value, MonitoredContainer monitoredContainer) {
        this.name = name;
        this.networkEntityName = networkEntityName;
        this.metric = metric;
        this.networkEntityInterfaceName = networkEntityInterfaceName;
        this.value = value;
        this.monitoredContainer = monitoredContainer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNetworkEntityName() {
        return networkEntityName;
    }

    public void setNetworkEntityName(String networkEntityName) {
        this.networkEntityName = networkEntityName;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public String getNetworkEntityInterfaceName() {
        return networkEntityInterfaceName;
    }

    public void setNetworkEntityInterfaceName(String networkEntityInterfaceName) {
        this.networkEntityInterfaceName = networkEntityInterfaceName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public MonitoredContainer getMonitoredContainer() {
        return monitoredContainer;
    }

    public void setMonitoredContainer(MonitoredContainer monitoredContainer) {
        this.monitoredContainer = monitoredContainer;
    }
}
