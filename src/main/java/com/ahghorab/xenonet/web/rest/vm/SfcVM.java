package com.ahghorab.xenonet.web.rest.vm;

public class SfcVM extends BaseEntityVM<Integer> {
    private String text;
    private String isGroup;
    private String category;
    private String key;

    public SfcVM(){
    }

    public SfcVM(String text, String isGroup, String category, String key) {
        this.text = text;
        this.isGroup = isGroup;
        this.category = category;
        this.key = key;
    }

    public SfcVM(Integer id, String createdDate, String updatedDate, String text, String isGroup, String category, String key) {
        super(id, createdDate, updatedDate);
        this.text = text;
        this.isGroup = isGroup;
        this.category = category;
        this.key = key;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getIsGroup() {
        return isGroup;
    }

    public void setIsGroup(String isGroup) {
        this.isGroup = isGroup;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
