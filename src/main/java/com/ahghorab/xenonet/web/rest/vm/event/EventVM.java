package com.ahghorab.xenonet.web.rest.vm.event;

import com.ahghorab.xenonet.web.rest.vm.BaseEntityVM;
import com.ahghorab.xenonet.web.rest.vm.event.exec.*;
import com.ahghorab.xenonet.web.rest.vm.event.logic.LogicalAndVM;
import com.ahghorab.xenonet.web.rest.vm.event.logic.LogicalOrVM;
import com.ahghorab.xenonet.web.rest.vm.event.math.*;
import com.ahghorab.xenonet.web.rest.vm.event.notifier.NotifierExternalVM;
import com.ahghorab.xenonet.web.rest.vm.event.notifier.NotifierSwitchVM;
import com.ahghorab.xenonet.web.rest.vm.event.notifier.NotifierVNFVM;

import java.util.List;

public class EventVM extends BaseEntityVM<Long> {
    private String name;
    private EventDiagramVM eventDiagram;
    private String status;
    private String numberOfOccur;
    private String eventJson;

    public EventVM() {
    }

    public EventVM(String name, EventDiagramVM eventDiagram, String status, String numberOfOccur, String eventJson) {
        this.name = name;
        this.eventDiagram = eventDiagram;
        this.status = status;
        this.numberOfOccur = numberOfOccur;
        this.eventJson = eventJson;
    }

    public EventVM(Long id, String createdDate, String updatedDate, String name, EventDiagramVM eventDiagram, String status, String numberOfOccur, String eventJson) {
        super(id, createdDate, updatedDate);
        this.name = name;
        this.eventDiagram = eventDiagram;
        this.status = status;
        this.numberOfOccur = numberOfOccur;
        this.eventJson = eventJson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EventDiagramVM getEventDiagram() {
        return eventDiagram;
    }

    public void setEventDiagram(EventDiagramVM eventDiagram) {
        this.eventDiagram = eventDiagram;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNumberOfOccur() {
        return numberOfOccur;
    }

    public void setNumberOfOccur(String numberOfOccur) {
        this.numberOfOccur = numberOfOccur;
    }

    public String getEventJson() {
        return eventJson;
    }

    public void setEventJson(String eventJson) {
        this.eventJson = eventJson;
    }
}
