package com.ahghorab.xenonet.web.rest.vm.cAdvisor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Container_stats {
    private String timestamp;
    @JsonProperty("cpu")
    Cpu cpuObject;
    @JsonProperty("diskio")
    Diskio diskioObject;
    @JsonProperty("memory")
    Memory memoryObject;
    @JsonProperty("network")
    Network networkObject;
//    @JsonProperty("filesystem")
    @JsonIgnore
    FileSystem filesystem;
    @JsonProperty("task_stats")
    Task_stats task_statsObject;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Cpu getCpuObject() {
        return cpuObject;
    }

    public void setCpuObject(Cpu cpuObject) {
        this.cpuObject = cpuObject;
    }

    public Diskio getDiskioObject() {
        return diskioObject;
    }

    public void setDiskioObject(Diskio diskioObject) {
        this.diskioObject = diskioObject;
    }

    public Memory getMemoryObject() {
        return memoryObject;
    }

    public void setMemoryObject(Memory memoryObject) {
        this.memoryObject = memoryObject;
    }

    public Network getNetworkObject() {
        return networkObject;
    }

    public void setNetworkObject(Network networkObject) {
        this.networkObject = networkObject;
    }

    public FileSystem getFilesystem() {
        return filesystem;
    }

    public void setFilesystem(FileSystem filesystem) {
        this.filesystem = filesystem;
    }

    public Task_stats getTask_statsObject() {
        return task_statsObject;
    }

    public void setTask_statsObject(Task_stats task_statsObject) {
        this.task_statsObject = task_statsObject;
    }
}
