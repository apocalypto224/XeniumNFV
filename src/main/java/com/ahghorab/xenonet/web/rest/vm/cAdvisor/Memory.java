package com.ahghorab.xenonet.web.rest.vm.cAdvisor;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Memory {
    @JsonProperty("usage")
    private float usage;
    @JsonProperty("cache")
    private float cache;
    @JsonProperty("rss")
    private float rss;
    @JsonProperty("working_set")
    private float working_set;
    @JsonProperty("failcnt")
    private float failcnt;
    @JsonProperty("container_data")
    Container_data container_data;
    @JsonProperty("hierarchical_data")
    Hierarchical_data hierarchical_data;

    public float getUsage() {
        return usage;
    }

    public void setUsage(float usage) {
        this.usage = usage;
    }

    public float getCache() {
        return cache;
    }

    public void setCache(float cache) {
        this.cache = cache;
    }

    public float getRss() {
        return rss;
    }

    public void setRss(float rss) {
        this.rss = rss;
    }

    public float getWorking_set() {
        return working_set;
    }

    public void setWorking_set(float working_set) {
        this.working_set = working_set;
    }

    public float getFailcnt() {
        return failcnt;
    }

    public void setFailcnt(float failcnt) {
        this.failcnt = failcnt;
    }

    public Container_data getContainer_data() {
        return container_data;
    }

    public void setContainer_data(Container_data container_data) {
        this.container_data = container_data;
    }

    public Hierarchical_data getHierarchical_data() {
        return hierarchical_data;
    }

    public void setHierarchical_data(Hierarchical_data hierarchical_data) {
        this.hierarchical_data = hierarchical_data;
    }
}

