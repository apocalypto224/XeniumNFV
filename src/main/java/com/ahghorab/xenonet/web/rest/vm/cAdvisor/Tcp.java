package com.ahghorab.xenonet.web.rest.vm.cAdvisor;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Tcp {
    @JsonProperty("Established")
    private float established;
    @JsonProperty("SynSent")
    private float synSent;
    @JsonProperty("SynRecv")
    private float synRecv;
    @JsonProperty("FinWait1")
    private float finWait1;
    @JsonProperty("FinWait2")
    private float finWait2;
    @JsonProperty("TimeWait")
    private float timeWait;
    @JsonProperty("Close")
    private float close;
    @JsonProperty("CloseWait")
    private float closeWait;
    @JsonProperty("LastAck")
    private float lastAck;
    @JsonProperty("Listen")
    private float listen;
    @JsonProperty("Closing")
    private float closing;

    public float getEstablished() {
        return established;
    }

    public void setEstablished(float established) {
        this.established = established;
    }

    public float getSynSent() {
        return synSent;
    }

    public void setSynSent(float synSent) {
        this.synSent = synSent;
    }

    public float getSynRecv() {
        return synRecv;
    }

    public void setSynRecv(float synRecv) {
        this.synRecv = synRecv;
    }

    public float getFinWait1() {
        return finWait1;
    }

    public void setFinWait1(float finWait1) {
        this.finWait1 = finWait1;
    }

    public float getFinWait2() {
        return finWait2;
    }

    public void setFinWait2(float finWait2) {
        this.finWait2 = finWait2;
    }

    public float getTimeWait() {
        return timeWait;
    }

    public void setTimeWait(float timeWait) {
        this.timeWait = timeWait;
    }

    public float getClose() {
        return close;
    }

    public void setClose(float close) {
        this.close = close;
    }

    public float getCloseWait() {
        return closeWait;
    }

    public void setCloseWait(float closeWait) {
        this.closeWait = closeWait;
    }

    public float getLastAck() {
        return lastAck;
    }

    public void setLastAck(float lastAck) {
        this.lastAck = lastAck;
    }

    public float getListen() {
        return listen;
    }

    public void setListen(float listen) {
        this.listen = listen;
    }

    public float getClosing() {
        return closing;
    }

    public void setClosing(float closing) {
        this.closing = closing;
    }
}
