package com.ahghorab.xenonet.web.rest.vm.cAdvisor;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Task_stats {
    @JsonProperty("nr_sleeping")
    private float nr_sleeping;
    @JsonProperty("nr_running")
    private float nr_running;
    @JsonProperty("nr_stopped")
    private float nr_stopped;
    @JsonProperty("nr_uninterruptible")
    private float nr_uninterruptible;
    @JsonProperty("nr_io_wait")
    private float nr_io_wait;


    // Getter Methods

    public float getNr_sleeping() {
        return nr_sleeping;
    }

    public float getNr_running() {
        return nr_running;
    }

    public float getNr_stopped() {
        return nr_stopped;
    }

    public float getNr_uninterruptible() {
        return nr_uninterruptible;
    }

    public float getNr_io_wait() {
        return nr_io_wait;
    }

    // Setter Methods

    public void setNr_sleeping( float nr_sleeping ) {
        this.nr_sleeping = nr_sleeping;
    }

    public void setNr_running( float nr_running ) {
        this.nr_running = nr_running;
    }

    public void setNr_stopped( float nr_stopped ) {
        this.nr_stopped = nr_stopped;
    }

    public void setNr_uninterruptible( float nr_uninterruptible ) {
        this.nr_uninterruptible = nr_uninterruptible;
    }

    public void setNr_io_wait( float nr_io_wait ) {
        this.nr_io_wait = nr_io_wait;
    }
}
