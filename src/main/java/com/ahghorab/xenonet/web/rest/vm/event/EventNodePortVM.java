package com.ahghorab.xenonet.web.rest.vm.event;

public class EventNodePortVM {
    private String portColor;
    private String portId;
    private String name;

    public EventNodePortVM() {
    }

    public EventNodePortVM(String portColor, String portId, String name) {
        this.portColor = portColor;
        this.portId = portId;
        this.name = name;
    }

    public String getPortColor() {
        return portColor;
    }

    public void setPortColor(String portColor) {
        this.portColor = portColor;
    }

    public String getPortId() {
        return portId;
    }

    public void setPortId(String portId) {
        this.portId = portId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
