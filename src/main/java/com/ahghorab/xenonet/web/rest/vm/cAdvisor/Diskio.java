package com.ahghorab.xenonet.web.rest.vm.cAdvisor;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Diskio {
    @JsonProperty("io_service_bytes")
    ArrayList<Object> io_service_bytes = new ArrayList<Object>();
    @JsonProperty("io_serviced")
    ArrayList<Object> io_serviced = new ArrayList<Object>();

    public ArrayList<Object> getIo_service_bytes() {
        return io_service_bytes;
    }

    public void setIo_service_bytes(ArrayList<Object> io_service_bytes) {
        this.io_service_bytes = io_service_bytes;
    }

    public ArrayList<Object> getIo_serviced() {
        return io_serviced;
    }

    public void setIo_serviced(ArrayList<Object> io_serviced) {
        this.io_serviced = io_serviced;
    }
}
