package com.ahghorab.xenonet.web.rest.vm.cAdvisor;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FileSystem {
    @JsonProperty("device")
    private String device;
    @JsonProperty("type")
    private String type;
    @JsonProperty("capacity")
    private float capacity;
    @JsonProperty("usage")
    private float usage;
    @JsonProperty("base_usage")
    private float base_usage;
    @JsonProperty("available")
    private float available;
    @JsonProperty("inodes_free")
    private float inodes_free;
    @JsonProperty("reads_completed")
    private float reads_completed;
    @JsonProperty("reads_merged")
    private float reads_merged;
    @JsonProperty("sectors_read")
    private float sectors_read;
    @JsonProperty("read_time")
    private float read_time;
    @JsonProperty("writes_completed")
    private float writes_completed;

    @JsonProperty("writes_merged")
    private float writes_merged;

    @JsonProperty("sectors_written")
    private float sectors_written;

    @JsonProperty("write_time")
    private float write_time;

    @JsonProperty("io_in_progress")
    private float io_in_progress;

    @JsonProperty("io_time")
    private float io_time;

    @JsonProperty("weighted_io_time")
    private float weighted_io_time;

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getCapacity() {
        return capacity;
    }

    public void setCapacity(float capacity) {
        this.capacity = capacity;
    }

    public float getUsage() {
        return usage;
    }

    public void setUsage(float usage) {
        this.usage = usage;
    }

    public float getBase_usage() {
        return base_usage;
    }

    public void setBase_usage(float base_usage) {
        this.base_usage = base_usage;
    }

    public float getAvailable() {
        return available;
    }

    public void setAvailable(float available) {
        this.available = available;
    }

    public float getInodes_free() {
        return inodes_free;
    }

    public void setInodes_free(float inodes_free) {
        this.inodes_free = inodes_free;
    }

    public float getReads_completed() {
        return reads_completed;
    }

    public void setReads_completed(float reads_completed) {
        this.reads_completed = reads_completed;
    }

    public float getReads_merged() {
        return reads_merged;
    }

    public void setReads_merged(float reads_merged) {
        this.reads_merged = reads_merged;
    }

    public float getSectors_read() {
        return sectors_read;
    }

    public void setSectors_read(float sectors_read) {
        this.sectors_read = sectors_read;
    }

    public float getRead_time() {
        return read_time;
    }

    public void setRead_time(float read_time) {
        this.read_time = read_time;
    }

    public float getWrites_completed() {
        return writes_completed;
    }

    public void setWrites_completed(float writes_completed) {
        this.writes_completed = writes_completed;
    }

    public float getWrites_merged() {
        return writes_merged;
    }

    public void setWrites_merged(float writes_merged) {
        this.writes_merged = writes_merged;
    }

    public float getSectors_written() {
        return sectors_written;
    }

    public void setSectors_written(float sectors_written) {
        this.sectors_written = sectors_written;
    }

    public float getWrite_time() {
        return write_time;
    }

    public void setWrite_time(float write_time) {
        this.write_time = write_time;
    }

    public float getIo_in_progress() {
        return io_in_progress;
    }

    public void setIo_in_progress(float io_in_progress) {
        this.io_in_progress = io_in_progress;
    }

    public float getIo_time() {
        return io_time;
    }

    public void setIo_time(float io_time) {
        this.io_time = io_time;
    }

    public float getWeighted_io_time() {
        return weighted_io_time;
    }

    public void setWeighted_io_time(float weighted_io_time) {
        this.weighted_io_time = weighted_io_time;
    }
}
