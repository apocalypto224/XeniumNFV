package com.ahghorab.xenonet.web.rest.vm;

public class NodePortVM {
    private String portColor;
    private String portId;
    private String name;
    private String ipAddress;
    private String gateway;

    public NodePortVM() {
    }

    public NodePortVM(String portColor, String portId, String name, String ipAddress, String gateway) {
        this.portColor = portColor;
        this.portId = portId;
        this.name = name;
        this.ipAddress = ipAddress;
        this.gateway = gateway;
    }

    public String getPortColor() {
        return portColor;
    }

    public void setPortColor(String portColor) {
        this.portColor = portColor;
    }

    public String getPortId() {
        return portId;
    }

    public void setPortId(String portId) {
        this.portId = portId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }
}
