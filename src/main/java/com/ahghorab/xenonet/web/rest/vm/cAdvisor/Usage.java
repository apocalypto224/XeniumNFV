package com.ahghorab.xenonet.web.rest.vm.cAdvisor;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Usage {
    @JsonProperty("total")
    private Float total;
    @JsonProperty("per_cpu_usage")
    ArrayList<Object> per_cpu_usage = new ArrayList<Object>();
    @JsonProperty("user")
    private Float user;
    @JsonProperty("system")
    private Float system;


    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public ArrayList<Object> getPer_cpu_usage() {
        return per_cpu_usage;
    }

    public void setPer_cpu_usage(ArrayList<Object> per_cpu_usage) {
        this.per_cpu_usage = per_cpu_usage;
    }

    public Float getUser() {
        return user;
    }

    public void setUser(Float user) {
        this.user = user;
    }

    public Float getSystem() {
        return system;
    }

    public void setSystem(Float system) {
        this.system = system;
    }
}
