package com.ahghorab.xenonet.web.rest.vm.cAdvisor;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContainerStatus {
    @JsonProperty("timestamp")
    private String timestamp;
    @JsonProperty("machine_name")
    private String machine_name;
    @JsonProperty("container_Name")
    private String container_Name;
    @JsonProperty("container_Id")
    private String container_Id;
    @JsonProperty("container_labels")
    Container_labels container_labelsObject;
    @JsonProperty("container_stats")
    Container_stats container_statsObject;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getMachine_name() {
        return machine_name;
    }

    public void setMachine_name(String machine_name) {
        this.machine_name = machine_name;
    }

    public String getContainer_Name() {
        return container_Name;
    }

    public void setContainer_Name(String container_Name) {
        this.container_Name = container_Name;
    }

    public String getContainer_Id() {
        return container_Id;
    }

    public void setContainer_Id(String container_Id) {
        this.container_Id = container_Id;
    }

    public Container_labels getContainer_labelsObject() {
        return container_labelsObject;
    }

    public void setContainer_labelsObject(Container_labels container_labelsObject) {
        this.container_labelsObject = container_labelsObject;
    }

    public Container_stats getContainer_statsObject() {
        return container_statsObject;
    }

    public void setContainer_statsObject(Container_stats container_statsObject) {
        this.container_statsObject = container_statsObject;
    }
}
