package com.ahghorab.xenonet.web.rest.vm.cAdvisor;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Tcp6 {
    @JsonProperty("Established")
    private float established;
    @JsonProperty("SynSent")
    private float SynSent;
    @JsonProperty("SynRecv")
    private float SynRecv;
    @JsonProperty("FinWait1")
    private float FinWait1;
    @JsonProperty("FinWait2")
    private float FinWait2;
    @JsonProperty("TimeWait")
    private float TimeWait;
    @JsonProperty("Close")
    private float Close;
    @JsonProperty("CloseWait")
    private float CloseWait;
    @JsonProperty("LastAck")
    private float LastAck;
    @JsonProperty("Listen")
    private float Listen;
    @JsonProperty("Closing")
    private float Closing;

    public float getEstablished() {
        return established;
    }

    public void setEstablished(float established) {
        this.established = established;
    }

    public float getSynSent() {
        return SynSent;
    }

    public void setSynSent(float synSent) {
        SynSent = synSent;
    }

    public float getSynRecv() {
        return SynRecv;
    }

    public void setSynRecv(float synRecv) {
        SynRecv = synRecv;
    }

    public float getFinWait1() {
        return FinWait1;
    }

    public void setFinWait1(float finWait1) {
        FinWait1 = finWait1;
    }

    public float getFinWait2() {
        return FinWait2;
    }

    public void setFinWait2(float finWait2) {
        FinWait2 = finWait2;
    }

    public float getTimeWait() {
        return TimeWait;
    }

    public void setTimeWait(float timeWait) {
        TimeWait = timeWait;
    }

    public float getClose() {
        return Close;
    }

    public void setClose(float close) {
        Close = close;
    }

    public float getCloseWait() {
        return CloseWait;
    }

    public void setCloseWait(float closeWait) {
        CloseWait = closeWait;
    }

    public float getLastAck() {
        return LastAck;
    }

    public void setLastAck(float lastAck) {
        LastAck = lastAck;
    }

    public float getListen() {
        return Listen;
    }

    public void setListen(float listen) {
        Listen = listen;
    }

    public float getClosing() {
        return Closing;
    }

    public void setClosing(float closing) {
        Closing = closing;
    }
}
