package com.ahghorab.xenonet.web.rest.vm.event.exec;

import com.ahghorab.xenonet.web.rest.vm.BaseEntityVM;
import com.ahghorab.xenonet.web.rest.vm.event.EventDiagramEntityConnectionsVM;
import com.ahghorab.xenonet.web.rest.vm.event.EventNodePortVM;

import java.util.List;


//this.nodeId = nodeId ? nodeId : '';
//    this.bridgeName1 = bridgeName1 ? bridgeName1 : '';
//    this.bridgeName2 = bridgeName2 ? bridgeName2 : '';
public class CreateSwitchPatchPeerVM extends BaseEntityVM<Integer> {
    private String key;
    private String name;
    private String describtion;
    private String loc;
    private List<EventNodePortVM> leftArray;
    private List<EventNodePortVM>  topArray;
    private List<EventNodePortVM> bottomArray;
    private List<EventNodePortVM>  rightArray;
    private String group;
    private String category;
    private String priorityNum;
    private String value;
    private String valueType;
    private String nodeId;
    private String bridgeName1;
    private String bridgeName2;
    private List<EventDiagramEntityConnectionsVM> eventDiagramEntityConnections;

    public CreateSwitchPatchPeerVM() {
    }

    public CreateSwitchPatchPeerVM(String key, String name, String describtion, String loc, List<EventNodePortVM> leftArray, List<EventNodePortVM> topArray, List<EventNodePortVM> bottomArray, List<EventNodePortVM> rightArray, String group, String category, String priorityNum, String value, String valueType, String nodeId, String bridgeName1, String bridgeName2, List<EventDiagramEntityConnectionsVM> eventDiagramEntityConnections) {
        this.key = key;
        this.name = name;
        this.describtion = describtion;
        this.loc = loc;
        this.leftArray = leftArray;
        this.topArray = topArray;
        this.bottomArray = bottomArray;
        this.rightArray = rightArray;
        this.group = group;
        this.category = category;
        this.priorityNum = priorityNum;
        this.value = value;
        this.valueType = valueType;
        this.nodeId = nodeId;
        this.bridgeName1 = bridgeName1;
        this.bridgeName2 = bridgeName2;
        this.eventDiagramEntityConnections = eventDiagramEntityConnections;
    }

    public CreateSwitchPatchPeerVM(Integer id, String createdDate, String updatedDate, String key, String name, String describtion, String loc, List<EventNodePortVM> leftArray, List<EventNodePortVM> topArray, List<EventNodePortVM> bottomArray, List<EventNodePortVM> rightArray, String group, String category, String priorityNum, String value, String valueType, String nodeId, String bridgeName1, String bridgeName2, List<EventDiagramEntityConnectionsVM> eventDiagramEntityConnections) {
        super(id, createdDate, updatedDate);
        this.key = key;
        this.name = name;
        this.describtion = describtion;
        this.loc = loc;
        this.leftArray = leftArray;
        this.topArray = topArray;
        this.bottomArray = bottomArray;
        this.rightArray = rightArray;
        this.group = group;
        this.category = category;
        this.priorityNum = priorityNum;
        this.value = value;
        this.valueType = valueType;
        this.nodeId = nodeId;
        this.bridgeName1 = bridgeName1;
        this.bridgeName2 = bridgeName2;
        this.eventDiagramEntityConnections = eventDiagramEntityConnections;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribtion() {
        return describtion;
    }

    public void setDescribtion(String describtion) {
        this.describtion = describtion;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public List<EventNodePortVM> getLeftArray() {
        return leftArray;
    }

    public void setLeftArray(List<EventNodePortVM> leftArray) {
        this.leftArray = leftArray;
    }

    public List<EventNodePortVM> getTopArray() {
        return topArray;
    }

    public void setTopArray(List<EventNodePortVM> topArray) {
        this.topArray = topArray;
    }

    public List<EventNodePortVM> getBottomArray() {
        return bottomArray;
    }

    public void setBottomArray(List<EventNodePortVM> bottomArray) {
        this.bottomArray = bottomArray;
    }

    public List<EventNodePortVM> getRightArray() {
        return rightArray;
    }

    public void setRightArray(List<EventNodePortVM> rightArray) {
        this.rightArray = rightArray;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPriorityNum() {
        return priorityNum;
    }

    public void setPriorityNum(String priorityNum) {
        this.priorityNum = priorityNum;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getBridgeName1() {
        return bridgeName1;
    }

    public void setBridgeName1(String bridgeName1) {
        this.bridgeName1 = bridgeName1;
    }

    public String getBridgeName2() {
        return bridgeName2;
    }

    public void setBridgeName2(String bridgeName2) {
        this.bridgeName2 = bridgeName2;
    }

    public List<EventDiagramEntityConnectionsVM> getEventDiagramEntityConnections() {
        return eventDiagramEntityConnections;
    }

    public void setEventDiagramEntityConnections(List<EventDiagramEntityConnectionsVM> eventDiagramEntityConnections) {
        this.eventDiagramEntityConnections = eventDiagramEntityConnections;
    }
}
