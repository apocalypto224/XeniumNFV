package com.ahghorab.xenonet.web.rest.vm.cAdvisor;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Cpu {
    @JsonProperty("usage")
    Usage usage;
    @JsonProperty("load_average")
    private float load_average;

    public Usage getUsage() {
        return usage;
    }

    public void setUsage(Usage usage) {
        this.usage = usage;
    }

    public float getLoad_average() {
        return load_average;
    }

    public void setLoad_average(float load_average) {
        this.load_average = load_average;
    }
}
