package com.ahghorab.xenonet.web.rest.vm.cAdvisor;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Container_data {
    @JsonProperty("pgfault")
    private float pgfault;
    @JsonProperty("pgmajfault")
    private float pgmajfault;


    // Getter Methods

    public float getPgfault() {
        return pgfault;
    }

    public float getPgmajfault() {
        return pgmajfault;
    }

    // Setter Methods

    public void setPgfault( float pgfault ) {
        this.pgfault = pgfault;
    }

    public void setPgmajfault( float pgmajfault ) {
        this.pgmajfault = pgmajfault;
    }
}
