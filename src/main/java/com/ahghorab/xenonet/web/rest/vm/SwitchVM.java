package com.ahghorab.xenonet.web.rest.vm;

import java.util.List;

public class SwitchVM extends BaseEntityVM<Integer> {
    private String source;
    private String key;
    private String name;
    private String describtion;
    private String loc;
    private List<NodePortVM> leftArray;
    private List<NodePortVM>  topArray;
    private List<NodePortVM> bottomArray;
    private List<NodePortVM>  rightArray;
    private String group;
    private String serverId;
    private String category;
    private String controllerIp;
    private String controllerPort;
    private List<DiagramEntityConnectionsVM> diagramEntityConnections;

    public SwitchVM(){

    }

    public SwitchVM(String source, String key, String name, String describtion, String loc, List<NodePortVM> leftArray, List<NodePortVM> topArray, List<NodePortVM> bottomArray, List<NodePortVM> rightArray, String group, String serverId, String category, String controllerIp, String controllerPort, List<DiagramEntityConnectionsVM> diagramEntityConnections) {
        this.source = source;
        this.key = key;
        this.name = name;
        this.describtion = describtion;
        this.loc = loc;
        this.leftArray = leftArray;
        this.topArray = topArray;
        this.bottomArray = bottomArray;
        this.rightArray = rightArray;
        this.group = group;
        this.serverId = serverId;
        this.category = category;
        this.controllerIp = controllerIp;
        this.controllerPort = controllerPort;
        this.diagramEntityConnections = diagramEntityConnections;
    }

    public SwitchVM(Integer id, String createdDate, String updatedDate, String source, String key, String name, String describtion, String loc, List<NodePortVM> leftArray, List<NodePortVM> topArray, List<NodePortVM> bottomArray, List<NodePortVM> rightArray, String group, String serverId, String category, String controllerIp, String controllerPort, List<DiagramEntityConnectionsVM> diagramEntityConnections) {
        super(id, createdDate, updatedDate);
        this.source = source;
        this.key = key;
        this.name = name;
        this.describtion = describtion;
        this.loc = loc;
        this.leftArray = leftArray;
        this.topArray = topArray;
        this.bottomArray = bottomArray;
        this.rightArray = rightArray;
        this.group = group;
        this.serverId = serverId;
        this.category = category;
        this.controllerIp = controllerIp;
        this.controllerPort = controllerPort;
        this.diagramEntityConnections = diagramEntityConnections;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribtion() {
        return describtion;
    }

    public void setDescribtion(String describtion) {
        this.describtion = describtion;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public List<NodePortVM> getLeftArray() {
        return leftArray;
    }

    public void setLeftArray(List<NodePortVM> leftArray) {
        this.leftArray = leftArray;
    }

    public List<NodePortVM> getTopArray() {
        return topArray;
    }

    public void setTopArray(List<NodePortVM> topArray) {
        this.topArray = topArray;
    }

    public List<NodePortVM> getBottomArray() {
        return bottomArray;
    }

    public void setBottomArray(List<NodePortVM> bottomArray) {
        this.bottomArray = bottomArray;
    }

    public List<NodePortVM> getRightArray() {
        return rightArray;
    }

    public void setRightArray(List<NodePortVM> rightArray) {
        this.rightArray = rightArray;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getControllerIp() {
        return controllerIp;
    }

    public void setControllerIp(String controllerIp) {
        this.controllerIp = controllerIp;
    }

    public String getControllerPort() {
        return controllerPort;
    }

    public void setControllerPort(String controllerPort) {
        this.controllerPort = controllerPort;
    }

    public List<DiagramEntityConnectionsVM> getDiagramEntityConnections() {
        return diagramEntityConnections;
    }

    public void setDiagramEntityConnections(List<DiagramEntityConnectionsVM> diagramEntityConnections) {
        this.diagramEntityConnections = diagramEntityConnections;
    }
}
