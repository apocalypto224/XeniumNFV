import {HttpInterceptor} from 'ng-jhipster';
import {RequestOptionsArgs, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {NotificationService} from '../../shared/utils/notification.service';
import {Injector} from '@angular/core';

export class NotificationInterceptor extends HttpInterceptor {

    private alertTxt: string;
    private notificationService: NotificationService;

    constructor(private injector: Injector) {
        super();
        setTimeout(() => (this.notificationService = injector.get(NotificationService)));
    }

    requestIntercept(options?: RequestOptionsArgs): RequestOptionsArgs {
        return options;
    }

    responseIntercept(observable: Observable<Response>): Observable<Response> {
        console.log('NotificationInterceptor');
        return observable.map((event) => {
            if (event.status > 300) {
                const arr = event.headers.keys();
                let alertParams = null;
                arr.forEach(entry => {
                    if (entry.endsWith('-alert')) {
                        this.alertTxt = event.headers.get(entry);
                    } else if (entry.endsWith('app-params')) {
                        alertParams = event.headers.get(entry);
                    }
                });
                if (this.alertTxt) {
                    this.notificationService.smallBox({
                        title: this.alertTxt,
                        content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
                        color: '#FF9900',
                        iconSmall: 'fa fa-exclamation-triangle  bounce animated',
                        timeout: 8000
                    });
                }
            }
            return event;
        }).catch((error) => {
            return Observable.throw(error); // here, response is an error
        });
    }
}
