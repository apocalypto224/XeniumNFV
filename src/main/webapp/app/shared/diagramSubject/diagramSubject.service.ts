import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';


@Injectable()
export class DiagramSubjectService {

    public serverIdDiagraramMap = new Map<string, any>();
    // public serverIdDiagraramSubject = new Subject<Map<string, any>>();

    constructor() {
    }

    updateDiagrams(serverId: string, diagramModel: any) {
        this.serverIdDiagraramMap.set(serverId, diagramModel);
        // this.serverIdDiagraramSubject.next(this.serverIdDiagraramMap);
    }

}
