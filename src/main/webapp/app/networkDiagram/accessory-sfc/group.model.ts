/*
*
*    @ AH.GHORAB
*
*/
import {BaseEntity} from '../../shared/baseEntity/baseEntity.model';
import {DiagramEntityConnection} from '../tab-diagram-drawer/diagramEntityConnection.model';

export class Group extends BaseEntity<number> {
    constructor(
        public text?: string,
        public isGroup?: string,
        public category?: string,
        public key?: string,
    ) {
        super();
        this.id = -1;
        this.text = text ? text : '';
        this.key = key ? key : '';
        this.category = category ? category : '';
        this.isGroup = isGroup ? isGroup : '';
    }
}
