import {AfterViewInit, Component, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {TabsComponent} from './tabs-diagram/tabs-diagram.component';
import {Server} from '../servers/server.model';
import {ActivatedRoute} from '@angular/router';
import {ParseLinks} from 'ng-jhipster';
import {ServerService} from '../servers/server.service';
import {ResponseWrapper} from '../shared/model/response-wrapper.model';

import {DiagramOverView} from './diagram-overView/diagram-overView.component';
import {TabDiagramDrawer} from './tab-diagram-drawer/tab-diagram-drawer.component';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {NetworkDiagramService} from './networkDiagram.service';
import {NotificationService} from '../shared/utils/notification.service';
import {DiagramSubjectService} from '../shared/diagramSubject/diagramSubject.service';


export class ServerIdIp {
    constructor(public serverId: string, public serverPrimeIp: string) {
        this.serverId = serverId ? serverId : '';
        this.serverPrimeIp = serverPrimeIp ? serverPrimeIp : '';
    }
}


@Component({
    selector: 'app-networkDiagram',
    templateUrl: './networkDiagram.component.html'
})
export class NetworkDiagramComponent implements OnInit, AfterViewInit {
    public state: any = {
        tabs: {
            demo3: 'hr1'
        }
    };
    // 'Registering Servers Inventory'
    private spinnerText: string;

    @ViewChild('tabDiagramDrawer') tabDiagramDrawerTemplate;

    @ViewChildren('tabDiagramDrawer') tabDiagramDrawers: QueryList<TabDiagramDrawer>;

    @ViewChild('diagramOverView') networkDiagramOverViewRef: DiagramOverView;
    @ViewChild(TabsComponent) tabsComponent;

    public servers: Server[];
    currentAccount: any;
    error: any;
    success: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    itemsPerPage: any;

    constructor(private serverService: ServerService,
                private parseLinks: ParseLinks,
                private activatedRoute: ActivatedRoute,
                private networkDiagramService: NetworkDiagramService,
                private notificationService: NotificationService,
                private ng4LoadingSpinnerService: Ng4LoadingSpinnerService,
                private diagramSubjectService: DiagramSubjectService) {
        this.itemsPerPage = 100;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.loadAll();
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        // this.createDiagramDrawer('ubuntu server1');
    }

    loadAll() {
        this.spinnerText = 'Loading Servers ...';
        this.ng4LoadingSpinnerService.show();
        this.serverService
            .query('/getAll', {
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()
            })
            .subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(servers, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.servers = servers;
        // this.createServerStatusWidget(this.servers);
        this.createDiagramDrawer(this.servers);
    }

    private onError(error) {
        this.ng4LoadingSpinnerService.hide();
        this.spinnerText = '';
    }

    createDiagramDrawer(servers: Server[]) {
        let serversIdIp: ServerIdIp[] = new Array();
        servers.forEach(server => {
            server.networkCards.forEach(nic => {
                if (nic.isPrimary) {
                    serversIdIp.push(new ServerIdIp(server.id.toString(), nic.ipAddress));
                }
            });
            // Create Tabs for each Server
            this.tabsComponent.openTab(
                server.name,
                this.tabDiagramDrawerTemplate,
                server,
                false
            );
        });

        this.spinnerText = 'Register Servers to ovsdb inventory ...';

        if (servers.length > 0) {
            if (this.diagramSubjectService.serverIdDiagraramMap.size < 1) {
                // init "this.diagramSubjectService.serverIdDiagraramMap "
                servers.forEach(server => {
                    this.diagramSubjectService.updateDiagrams(server.id.toString(), '');
                });
                //Register servers to ovsdb invetory
                this.networkDiagramService.registerServersToInventory('/registerServersToInventory', serversIdIp)
                    .subscribe(
                        (res: ResponseWrapper) => {
                            this.ng4LoadingSpinnerService.hide();
                            this.spinnerText = '';
                            this.notificationService.smallBox({
                                title: 'Servers Registered to Inventory',
                                content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
                                color: '#16A085',
                                iconSmall: 'fa fa-check bounce animated',
                                timeout: 6000
                            });
                        },
                        (res: ResponseWrapper) => {
                            this.ng4LoadingSpinnerService.hide();
                            this.spinnerText = '';
                            this.notificationService.smallBox({
                                title: res ? res : 'Undefined Error',
                                content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
                                color: '#a00002',
                                iconSmall: 'fa fa-times  bounce animated',
                                timeout: 8000
                            });
                        }
                    );
            }
            this.ng4LoadingSpinnerService.hide();
            this.spinnerText = '';
        }


    }

    test() {
        // console.log(this.tabDiagramDrawers);
        // this.tabDiagramDrawers.for
        // .saveTest();
        // this.networkDiagramOverViewRef.save();
    }
}
