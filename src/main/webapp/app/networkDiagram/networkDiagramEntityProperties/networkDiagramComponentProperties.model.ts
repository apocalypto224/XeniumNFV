import {NodePort} from '../accessory-vnfs/nodePort.model';

export class NetworkDiagramComponentProperties {
    constructor(public name?: string,
                public cpu?: string,
                public ram?: string,
                public isDedicatedRes?: boolean,
                public nodePort?: NodePort[]) {
        this.name = name ? name : '';
        this.cpu = cpu ? cpu : '0';
        this.ram = ram ? ram : '0';
        this.isDedicatedRes = isDedicatedRes ? isDedicatedRes : false;
        this.nodePort = nodePort ? nodePort : [];
    }
}
