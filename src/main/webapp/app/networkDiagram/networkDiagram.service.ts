/*
*
*    @ AH.GHORAB
*
*/
import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';

import {GenericService} from '../shared/genericService/genericService';
import {Topology} from './tab-diagram-drawer/topology.model';
import {Observable} from 'rxjs/Rx';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ServerIdIp} from './networkDiagram.component';

@Injectable()
export class NetworkDiagramService extends GenericService<Topology> {
    private isOpen = false;

    constructor(http: Http,
                private modalService: NgbModal,) {
        super(http, '/api/networkDiagram');
    }

    removeDiagram(restOfUrl: string, id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl + restOfUrl}/${id}`);
    }

    registerServersToInventory(restOfUrl: string, serversIdIP: ServerIdIp[]):Observable<Response> {
        console.log(serversIdIP);
        return this.http.post(this.resourceUrl + restOfUrl, serversIdIP);
    }

    initDiagram(): Observable<Response>{

        return this.http.delete(`${this.resourceUrl + "/init"}`);
    }

    openModal(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        /* Damn Important
         USE THIS SERVICE HERE ,NEITHER IN RESOLVER NOR POPUP-OPENER-COMPONENT ---->
         OTHERWISE IT'LL RISE ERROR --> HERE IS PART OF ERROR :
         " ERROR Error: ExpressionChangedAfterItHasBeenCheckedError: Expression has changed
         after it was checked. Previous value: 'undefined'. Current value: 'true'.
         It seems like the view has been created after its parent and its children
         have been dirty checked. Has it been created in a change detection hook ? "
         */
        // this.groupService.getActionList(id).subscribe((groupActionList) => {
        // this.serverModalRef(component, groupActionList);
        // });
        setTimeout(() => {
            this.serverModalRef(component, '1');
        }, 1000);

    }

    serverModalRef(component: Component, object: any): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        // modalRef.componentInstance.groupActionList = groupActionList;
        modalRef.result.then((result) => {
            this.isOpen = false;
        }, (reason) => {
            this.isOpen = false;
        });
        return modalRef;
    }
}
