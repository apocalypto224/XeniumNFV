import {NodePort} from '../accessory-vnfs/nodePort.model';

export class NetworkDiagramMainServerInterfaceProperties {
    constructor(public mainServerInterFaceIp?: string,
                public nodePort?: NodePort[]) {
        this.mainServerInterFaceIp = mainServerInterFaceIp ? mainServerInterFaceIp : '';
        this.nodePort = nodePort ? nodePort : [];
    }
}
