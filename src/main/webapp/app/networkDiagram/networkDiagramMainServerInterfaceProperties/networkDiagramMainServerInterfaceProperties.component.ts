import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NetworkDiagramMainServerInterfaceProperties} from './networkDiagramMainServerInterfaceProperties.model';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {NodePort} from '../accessory-vnfs/nodePort.model';

@Component({
    selector: 'Xnet-networkDiagramMainServerInterfacePropertiesComponent',
    styles: [],
    templateUrl: './networkDiagramMainServerInterfaceProperties.component.html'
})
export class NetworkDiagramMainServerInterfacePropertiesComponent implements OnInit {

    public networkCards: FormGroup;

    @Input() networkDiagramMainServerInterfaceProperties:NetworkDiagramMainServerInterfaceProperties;

    @Output() applyChangeToMainServerInterface = new EventEmitter();

    constructor(private formBuilder: FormBuilder) {

    }

    ngOnInit() {
        this.networkCards = this.formBuilder.group({
            itemRows: this.formBuilder.array([this.initItemRows()])
        });
        this.deleteRow(0);
        if (this.networkDiagramMainServerInterfaceProperties.nodePort.length > 0) {
            this.addNewRow(this.networkDiagramMainServerInterfaceProperties.nodePort);
        }
    }

    applyChange() {
        this.networkDiagramMainServerInterfaceProperties.nodePort = [];
        this.networkCards.value.itemRows.forEach((element) => {
            let newRow = true;
            for (let nodePort of this.networkDiagramMainServerInterfaceProperties.nodePort) {
                if (nodePort.name === element.itemKey) {
                    newRow = false;
                }
            }
            if (newRow && element.itemKey !== null &&  element.itemKey !== '' && element.itemIp !== null &&  element.itemIp !== '') {
                this.networkDiagramMainServerInterfaceProperties.nodePort.push(
                    new NodePort(
                        '#425e5c',
                        element.itemKey,
                        element.itemKey,
                        element.itemIp,
                        ''
                    )
                )
            }
        });
        this.applyChangeToMainServerInterface.emit(this.networkDiagramMainServerInterfaceProperties);
    }

    initItemRows() {
        return this.formBuilder.group({
            itemKey: [''],
            itemIp: ['']
        });
    }

    addNewRow(nodePorts: NodePort[]) {
        const control = <FormArray>this.networkCards.controls['itemRows'];
        if (nodePorts) {
            nodePorts.forEach(nodePort => {
                control.push(
                    this.formBuilder.group({
                        itemKey: nodePort.portId,
                        itemIp: nodePort.ipAddress
                    })
                );
            });
        }
        else {
            control.push(this.initItemRows());
        }
    }

    deleteRow(index: number) {
        const control = <FormArray>this.networkCards.controls['itemRows'];
        control.removeAt(index);
    }


    trackByIndex(index: number, obj: any): any {
        return index;
    }
}
