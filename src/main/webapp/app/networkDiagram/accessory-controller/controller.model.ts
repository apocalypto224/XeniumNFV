/*
*
*    @ AH.GHORAB
*
*/
import {BaseEntity} from '../../shared/baseEntity/baseEntity.model';
import {DiagramEntityConnection} from '../tab-diagram-drawer/diagramEntityConnection.model';
import {NodePort} from '../accessory-vnfs/nodePort.model';

export class Controller extends BaseEntity<number> {
    constructor(
        public source?: string,
        public key?: string,
        public name?: string,
        public describtion?: string,
        public loc?: string,
        public ipAddress?: string,
        public cpu?: string,
        public ram?: string,
        public isDedicatedRes?: string,
        public leftArray?: NodePort[],
        public topArray?: NodePort[],
        public bottomArray?: NodePort[],
        public rightArray?: NodePort[],
        public group ?: string,
        public serverId?: string,
        public category?: string,
        public diagramEntityConnections?: DiagramEntityConnection[]
    ) {
        super();
        this.id = -1;
        this.source = source ? source : '';
        this.key = key ? key : '';
        this.name = name ? name : '';
        this.describtion = describtion ? describtion : '';
        this.loc = loc ? loc : '';
        this.ipAddress = ipAddress ? ipAddress : '';
        this.cpu = cpu ? cpu : '';
        this.ram = ram ? ram : '';
        this.isDedicatedRes = isDedicatedRes ? isDedicatedRes : '';
        this.leftArray = leftArray ? leftArray : [];
        this.topArray = topArray ? topArray : [];
        this.bottomArray = bottomArray ? bottomArray : [];
        this.rightArray = rightArray ? rightArray : [];
        this.group = group? group : '';
        this.serverId = serverId? serverId : '';
        this.category = category? category : '';
        this.diagramEntityConnections = diagramEntityConnections ? diagramEntityConnections : new Array();
    }
}
