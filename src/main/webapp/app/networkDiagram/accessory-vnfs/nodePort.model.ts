/*
*
*    @ AH.GHORAB
*
*/

export class NodePort {
    constructor(public portColor?: string,
                public portId?: string,
                public name?: string,
                public ipAddress?: string,
                public gateway?: string
                ) {
        this.portColor = portColor ? portColor : '';
        this.portId = portId ? portId : '';
        this.name = name ? name : '';
        this.ipAddress = ipAddress ? ipAddress : '';
        this.gateway = gateway ? gateway : '';
    }
}
