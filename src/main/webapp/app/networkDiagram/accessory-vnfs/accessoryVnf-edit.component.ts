import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Vnf} from './vnf.model';
import {NetworkDiagramComponentProperties} from '../networkDiagramEntityProperties/networkDiagramComponentProperties.model';
import {NodePort} from './nodePort.model';
import {EventManager} from 'ng-jhipster';

@Component({
    selector: 'accessoryVnf-popup',
    templateUrl: './accessoryVnf-edit.component.html'
})
export class AccessoryVnfPopupComponent implements OnInit, OnDestroy {
    public networkCards: FormGroup;
    public objVnf = new Vnf();
    public isDedicatedres: boolean = false;

    // private newServer: Server = new Server();
    // private newPrimNetworkCard: NetworkCard = new NetworkCard();
    knOptions = {
        readOnly: false,
        size: 80,
        unit: '',
        textColor: '#000000',
        fontSize: '32',
        fontWeigth: '700',
        fontFamily: 'Roboto',
        valueformat: 'percent',
        step: 0.1,
        value: 0,
        max: 10,
        trackWidth: 19,
        barWidth: 20,
        trackColor: '#D8D8D8',
        barColor: '#16A085',
        displayInput:false,
        displayPrevious: false,
        // subText: {
        //     enabled: false,
        //     fontFamily: 'Verdana',
        //     font: '14',
        //     fontWeight: 'bold',
        //     text: 'Overall',
        //     color: '#000000',
        //     offset: 7
        // },
    };

    constructor(public activeModal: NgbActiveModal,
                private formBuilder: FormBuilder,
                private eventManager: EventManager) {
        this.objVnf.cpu = '0';
        this.objVnf.ram = '0';
    }

    ngOnInit() {
        this.networkCards = this.formBuilder.group({
            itemRows: this.formBuilder.array([this.initItemRows()])
        });
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.objVnf.isDedicatedRes = this.isDedicatedres? '1': '0';
        this.objVnf.category = 'Node';
        this.networkCards.value.itemRows.forEach(element => {
            // TOP
            if (element.itemside === '0') {
                let isCardNameDuplicated: boolean = false;
                this.objVnf.topArray.forEach(el => {
                    if (el.name === element.itemname) {
                        isCardNameDuplicated = true;
                    }
                });
                if (!isCardNameDuplicated && element.itemname !== null && element.itemname !== '') {
                    this.objVnf.topArray.push(new NodePort(
                        '#425e5c',
                        element.itemname,
                        element.itemname,
                        '',
                        ''
                        )
                    );
                }
            }
            // RIGHT
            else if (element.itemside === '1') {
                let isCardNameDuplicated: boolean = false;
                this.objVnf.rightArray.forEach(el => {
                    if (el.name === element.itemname) {
                        isCardNameDuplicated = true;
                    }
                });
                if (!isCardNameDuplicated && element.itemname !== null && element.itemname !== '') {
                    this.objVnf.rightArray.push(new NodePort(
                        '#425e5c',
                        element.itemname,
                        element.itemname,
                        '',
                        ''
                        )
                    );
                }
            }
            // BOTTOM
            else if (element.itemside === '2') {
                let isCardNameDuplicated: boolean = false;
                this.objVnf.bottomArray.forEach(el => {
                    if (el.name === element.itemname) {
                        isCardNameDuplicated = true;
                    }
                });
                if (!isCardNameDuplicated && element.itemname !== null && element.itemname !== '') {
                    this.objVnf.bottomArray.push(new NodePort(
                        '#425e5c',
                        element.itemname,
                        element.itemname,
                        '',
                        ''
                        )
                    );
                }
            }
            // LEFT
            else {
                let isCardNameDuplicated: boolean = false;
                this.objVnf.leftArray.forEach(el => {
                    if (el.name === element.itemname) {
                        isCardNameDuplicated = true;
                    }
                });
                if (!isCardNameDuplicated && element.itemname !== null && element.itemname !== '') {
                    this.objVnf.leftArray.push(new NodePort(
                        '#425e5c',
                        element.itemname,
                        element.itemname,
                        '',
                        ''
                        )
                    );
                }
            }
        });
        this.eventManager.broadcast({
            name: 'accessoryVnfEditEvent',
            content: this.objVnf
        });
        this.clear();
    }

    private onSaveSuccess(result) {
        this.activeModal.dismiss(result);
    }

    trackUserById(index: number, item: any) {
    }

    initItemRows() {
        return this.formBuilder.group({
            itemname: [''],
            itemside: ['']
        });
    }

    addNewRow() {
        const control = <FormArray>this.networkCards.controls['itemRows'];
        control.push(this.initItemRows());
    }

    deleteRow(index: number) {
        const control = <FormArray>this.networkCards.controls['itemRows'];
        control.removeAt(index);
    }

    ngOnDestroy() {

    }
}
