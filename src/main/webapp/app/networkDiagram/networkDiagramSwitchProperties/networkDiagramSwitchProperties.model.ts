export class NetworkDiagramSwitchProperties {
    constructor(public controllerIp?: string,
                public controllerPort?: string) {
        this.controllerIp = controllerIp ? controllerIp : '';
        this.controllerPort = controllerPort ? controllerPort : '0';
    }
}
