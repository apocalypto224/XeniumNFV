import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';

import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {EventManager} from 'ng-jhipster';
import {ServerPopupService} from './server-popup.service';
import {Server} from './server.model';
import {NetworkCard} from './networkCard.model';
import {ServerService} from './server.service';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {NotificationService} from '../shared/utils/notification.service';

@Component({
    selector: 'server-popup',
    templateUrl: './server-edit.component.html'
})
export class ServerPopupComponent implements OnInit, OnDestroy {
    public networkCards: FormGroup;

    private newServer: Server = new Server();
    private newPrimNetworkCard: NetworkCard = new NetworkCard();

    constructor(public activeModal: NgbActiveModal,
                private eventManager: EventManager,
                private formBuilder: FormBuilder,
                private serverService: ServerService,
                private notificationService: NotificationService,
                private ng4LoadingSpinnerService: Ng4LoadingSpinnerService) {
    }

    ngOnInit() {
        this.networkCards = this.formBuilder.group({
            itemRows: this.formBuilder.array([this.initItemRows()])
        });
        this.deleteRow(0);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.ng4LoadingSpinnerService.show();
        this.newPrimNetworkCard.isPrimary = true;
        console.log(this.newPrimNetworkCard);
        this.newServer.networkCards.push(this.newPrimNetworkCard);
        this.networkCards.value.itemRows.forEach(element => {
            let newNetworkCard: NetworkCard = new NetworkCard();
            newNetworkCard.isPrimary = false;
            newNetworkCard.name = element.itemname;
            this.newServer.networkCards.push(newNetworkCard);
        });
        console.log(this.newServer);
        this.serverService.save('/new', this.newServer).subscribe((res: any) => {
            this.onSaveSuccess();
        }, (err) => {
            console.log('on server create eeeeeerrrrorororor');
            console.log(err);
            this.ng4LoadingSpinnerService.hide();
            this.notificationService.smallBox({
                title: err ? err : 'Undefined Error',
                content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
                color: '#a00002',
                iconSmall: 'fa fa-times  bounce animated',
                timeout: 8000
            });
        });

    }

    private onSaveSuccess() {
        this.ng4LoadingSpinnerService.hide();
        this.eventManager.broadcast({
            name: 'serverEditEvent',
            content: ''
        });
        this.clear();
    }

    trackUserById(index: number, item: any) {
    }

    initItemRows() {
        return this.formBuilder.group({
            itemname: ['']
        });
    }

    addNewRow() {
        const control = <FormArray>this.networkCards.controls['itemRows'];
        control.push(this.initItemRows());
    }

    deleteRow(index: number) {
        const control = <FormArray>this.networkCards.controls['itemRows'];
        control.removeAt(index);
    }

    ngOnDestroy() {
        // this.eventManager.broadcast({
        //     name: EventName.serverPopup,
        //     content: ''
        // });
    }
}

@Component({
    selector: 'server-popup-openner',
    template: ''
})
export class ServerPopupOpennerComponent implements OnInit, OnDestroy {
    modalRef: NgbModalRef;
    routeSub: any;

    constructor(private route: ActivatedRoute,
                private serverPopupService: ServerPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if (params['id']) {
                this.modalRef = this.serverPopupService.open(
                    ServerPopupComponent as Component,
                    params['id']
                );
            } else {
                this.modalRef = this.serverPopupService.open(
                    ServerPopupComponent as Component
                );
            }
        });
    }

    ngOnDestroy() {
    }
}
