import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Switch} from '../../networkDiagram/accessory-switch/swicth.model';
import {Server} from '../../servers/server.model';
import {DeleteBridgeProperties} from './deleteBridgeProperties.model';

const uuidv4 = require('uuid/v4');
@Component({
    selector: "Xnet-execDeleteBridgePropertiesComponent",
    styles: [],
    templateUrl: "./execDeleteBridgeProperties.component.html"
})
export class ExecDeleteBridgePropertiesComponent implements OnInit{
    @Input() deleteBridgeProperties: DeleteBridgeProperties;
    @Input() servers: Server[];
    @Output() applyChange = new EventEmitter();

    constructor() {}

    ngOnInit(){

    }

    emitApplyChange() {
        this.applyChange.emit(this.deleteBridgeProperties);
    }
}
