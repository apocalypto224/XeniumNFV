import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Server} from '../../servers/server.model';
import {UpdateVnfProperties} from './updateVnfProperties.model';

@Component({
    selector: "Xnet-updateVnfPropertiesComponent",
    styles: [],
    templateUrl: "./updateVnfProperties.component.html"
})
export class  UpdateVnfPropertiesComponent{
    @Input() updateVnfProperties: UpdateVnfProperties;
    @Input() servers: Server[];
    @Output() applyChange = new EventEmitter();

    knOptions = {
        readOnly: false,
        size: 80,
        unit: '',
        textColor: '#000000',
        fontSize: '32',
        fontWeigth: '700',
        fontFamily: 'Roboto',
        valueformat: 'percent',
        step: 0.1,
        value: 0,
        max: 10,
        trackWidth: 19,
        barWidth: 20,
        trackColor: '#D8D8D8',
        barColor: '#16A085',
        displayInput: false,
        displayPrevious: false
    };

    constructor() {}

    emitApplyChange() {
        this.applyChange.emit(this.updateVnfProperties);
    }
}
