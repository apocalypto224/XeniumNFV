import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Switch} from '../../networkDiagram/accessory-switch/swicth.model';
import {Server} from '../../servers/server.model';

const uuidv4 = require('uuid/v4');
@Component({
    selector: "Xnet-execCreateBridgePropertiesComponent",
    styles: [],
    templateUrl: "./execCreateBridgeProperties.component.html"
})
export class ExecCreateBridgePropertiesComponent implements OnInit{
    @Input() swch: Switch;
    @Input() servers: Server[];
    @Output() applyChange = new EventEmitter();

    constructor() {}

    ngOnInit(){

    }

    emitApplyChange() {
        this.applyChange.emit(this.swch);
    }
}
