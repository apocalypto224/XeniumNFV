export class CreateVnfPortProperties {
    constructor(public serverId?: string,
                public bridgeName?: string,
                public containerName?: string,
                public interfaceName?: string,
                public ipAddress?: string,
                public gateway?: string
    ) {
        this.serverId = serverId ? serverId : '';
        this.bridgeName = bridgeName ? bridgeName : '';
        this.containerName = containerName ? containerName : '';
        this.interfaceName = interfaceName ? interfaceName : '';
        this.ipAddress = ipAddress ? ipAddress : '';
        this.gateway = gateway ? gateway : '';
    }
}
