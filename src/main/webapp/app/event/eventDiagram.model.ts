/*
*
*    @ AH.GHORAB
*
*/
import {NotifierExternal} from './eventNodes/notifier/notifierExternal.model';
import {NotifierSwitch} from './eventNodes/notifier/notifierSwitch.model';
import {NotifierVNF} from './eventNodes/notifier/notifierVNF.model';
import {LogicalAnd} from './eventNodes/logic/logicalAnd.model';
import {LogicalOr} from './eventNodes/logic/logicalOr.model';
import {MathAdd} from './eventNodes/math/mathAdd.model';
import {MathCompare} from './eventNodes/math/mathCompare.model';
import {MathDiv} from './eventNodes/math/mathDiv.model';
import {MathMultiple} from './eventNodes/math/mathMultiple.model';
import {MathStaticVal} from './eventNodes/math/mathStaticVal.model';
import {CreateOdl} from './eventNodes/exec/createOdl.model';
import {CreateSwitch} from './eventNodes/exec/createSwitch.model';
import {CreateVnf} from './eventNodes/exec/createVnf.model';
import {CreateVnfPort} from './eventNodes/exec/createVnfPort.model';
import {DeleteOdl} from './eventNodes/exec/deleteOdl.model';
import {DeleteSwitch} from './eventNodes/exec/deleteSwitch.model';
import {DeleteVnf} from './eventNodes/exec/deleteVnf.model';
import {DeleteVnfPort} from './eventNodes/exec/deleteVnfPort.model';
import {UpdateVnf} from './eventNodes/exec/updateVnf.model';
import {EventDiagramEntityConnection} from './eventNodes/eventDiagramEntityConnection.model';
import {MathSub} from './eventNodes/math/mathSub.model';
import {CreateSwitchPatchPeer} from './eventNodes/exec/createSwitchPatchPeer.model';
import {DeleteSwitchPatchPeer} from './eventNodes/exec/deleteSwitchPatchPeer.model';

export class EventDiagram {
    constructor(
                public maxPriorityNum?: string,
                public notifierExternal?: NotifierExternal[],
                public notifierSwitch?: NotifierSwitch[],
                public notifierVnf?: NotifierVNF[],
                public logicAnd?: LogicalAnd[],
                public logicOr?: LogicalOr[],
                public mathAdd?: MathAdd[],
                public mathCompare?: MathCompare[],
                public mathDiv?: MathDiv[],
                public mathMultiple?: MathMultiple[],
                public mathStaticVal?: MathStaticVal[],
                public mathSub?: MathSub[],
                public createOdl?: CreateOdl[],
                public createSwitch?: CreateSwitch[],
                public createSwitchPatchPeer?: CreateSwitchPatchPeer[],
                public createVnf?: CreateVnf[],
                public createVnfPort?: CreateVnfPort[],
                public deleteOdl?: DeleteOdl[],
                public deleteSwitch?: DeleteSwitch[],
                public deleteSwitchPatchPeer?: DeleteSwitchPatchPeer[],
                public deleteVnf?: DeleteVnf[],
                public deleteVnfPort?: DeleteVnfPort[],
                public updateVnf?: UpdateVnf[],
                public eventDiagramEntityConnection?: EventDiagramEntityConnection[],) {
        this.maxPriorityNum = maxPriorityNum ? maxPriorityNum : '';
        this.notifierExternal = notifierExternal ? notifierExternal : [];
        this.notifierSwitch = notifierSwitch ? notifierSwitch : [];
        this.notifierVnf = notifierVnf ? notifierVnf : [];
        this.logicAnd = logicAnd ? logicAnd : [];
        this.logicOr = logicOr ? logicOr : [];
        this.mathAdd = mathAdd ? mathAdd : [];
        this.mathCompare = mathCompare ? mathCompare : [];
        this.mathDiv = mathDiv ? mathDiv : [];
        this.mathMultiple = mathMultiple ? mathMultiple : [];
        this.mathStaticVal = mathStaticVal ? mathStaticVal : [];
        this.mathSub = mathSub ? mathSub : [];
        this.createOdl = createOdl ? createOdl : [];
        this.createSwitch = createSwitch ? createSwitch : [];
        this.createSwitchPatchPeer = createSwitchPatchPeer ? createSwitchPatchPeer : [];
        this.createVnf = createVnf ? createVnf : [];
        this.createVnfPort = createVnfPort ? createVnfPort : [];
        this.deleteOdl = deleteOdl ? deleteOdl : [];
        this.deleteSwitch = deleteSwitch ? deleteSwitch : [];
        this.deleteSwitchPatchPeer = deleteSwitchPatchPeer ? deleteSwitchPatchPeer : [];
        this.deleteVnf = deleteVnf ? deleteVnf : [];
        this.deleteVnfPort = deleteVnfPort ? deleteVnfPort : [];
        this.updateVnf = updateVnf ? updateVnf : [];
        this.eventDiagramEntityConnection = eventDiagramEntityConnection ? eventDiagramEntityConnection : [];
    }
}
