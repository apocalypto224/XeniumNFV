import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MathCompareProperties} from './mathCompareProperties.model';

@Component({
    selector: "Xnet-mathComparePropertiesComponent",
    styles: [],
    templateUrl: "./mathCompareProperties.component.html"
})
export class MathComparePropertiesComponent {
    @Input()
    mathCompareProperties: MathCompareProperties;
    @Output() applyChange = new EventEmitter();


    constructor() {}

    emitApplyChange() {
        this.applyChange.emit(this.mathCompareProperties);
    }
}
