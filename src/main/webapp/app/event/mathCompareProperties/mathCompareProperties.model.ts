export class MathCompareProperties {
    constructor(public operation?: string) {
        this.operation = operation ? operation : '';
    }
}
