import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NotifierExternalProperties} from './notifierExternalProperties.model';

@Component({
    selector: "Xnet-notifierExternalPropertiesComponent",
    styles: [],
    templateUrl: "./notifierExternalProperties.component.html"
})
export class NotifierExternalPropertiesComponent {
    @Input()
    notifierExternalProperties: NotifierExternalProperties;
    @Output() applyChange = new EventEmitter();


    constructor() {}

    emitApplyChange() {
        this.applyChange.emit(this.notifierExternalProperties);
    }
}
