export class NotifierExternalProperties {
    constructor(public topicName?: string,
                public valueType?: string,
                public isLatch?: boolean) {
        this.topicName = topicName ? topicName : '';
        this.valueType = valueType ? valueType : 'BOOLEAN';
        this.isLatch = isLatch ? isLatch : false;
    }
}
