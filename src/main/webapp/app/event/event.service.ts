/*
*
*    @ AH.GHORAB
*
*/
import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';

import {GenericService} from '../shared/genericService/genericService';
import {Event} from './event.model';
import {Observable} from 'rxjs/Rx';
import {ResponseWrapper} from '../shared/model/response-wrapper.model';
import {createRequestOption} from '../shared/model/request-util';

@Injectable()
export class EventService extends GenericService<Event>{
    constructor(http: Http) {
        super(http, "/api/event");
    }
    stopEvent(id: number): Observable<Response> {
        return this.http.post(`${this.resourceUrl + '/stop'}/${id}`,'');
    }
    startEvent(id: number): Observable<Response> {
        return this.http.post(`${this.resourceUrl + '/start'}/${id}`,'');
    }

    getAllFrequently(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        // return this.http
        //     .get(this.resourceUrl + "/getAll", options)
        //     .map((res: Response) => this.convertResponse(res));

        return Observable
            .interval(5000)
            .flatMap((i) => this.http
            .get(this.resourceUrl + "/getAll", options)
            .map((res: Response) => this.convertResponse(res)));

    }
}
