/*
*
*    @ AH.GHORAB
*
*/
import {BaseEntity} from '../../../shared/baseEntity/baseEntity.model';
import {EventNodePort} from '../eventNodePort.model';
import {EventDiagramEntityConnection} from '../eventDiagramEntityConnection.model';

export class MathStaticVal extends BaseEntity<number> {
    constructor(
        public key?: string,
        public name?: string,
        public describtion?: string,
        public loc?: string,
        public leftArray?: EventNodePort[],
        public topArray?: EventNodePort[],
        public bottomArray?: EventNodePort[],
        public rightArray?: EventNodePort[],
        public group?: string,
        public category?: string,
        public priorityNum?: string,
        public value?: string,
        public valueType?: string,
        public eventDiagramEntityConnections?: EventDiagramEntityConnection[]
    ) {
        super();
        this.id = -1;
        this.key = key ? key : '';
        this.name = name ? name : '';
        this.describtion = describtion ? describtion : '';
        this.loc = loc ? loc : '';
        this.leftArray = leftArray ? leftArray : [];
        this.topArray = topArray ? topArray : [];
        this.bottomArray = bottomArray ? bottomArray : [];
        this.rightArray = rightArray ? rightArray : [];
        this.group = group? group : '';
        this.category = category? category : '';
        this.priorityNum = priorityNum ? priorityNum : '';
        this.value = value ? value : '';
        this.valueType = valueType ? valueType : '';
        this.eventDiagramEntityConnections = eventDiagramEntityConnections ? eventDiagramEntityConnections : new Array();
    }
}
