/*
*
*    @ AH.GHORAB
*
*/

export class EventNodePort {
    constructor(public portColor?: string,
                public portId?: string,
                public name?: string,
                // public value?: string,
                // public valueType?: string,
                // public priorityNum?: string
                ) {
        this.portColor = portColor ? portColor : '';
        this.portId = portId ? portId : '';
        this.name = name ? name : '';
        // this.value = value ? value : '';
        // this.valueType = valueType ? valueType : '';
        // this.priorityNum = priorityNum ? priorityNum : '';
    }
}
