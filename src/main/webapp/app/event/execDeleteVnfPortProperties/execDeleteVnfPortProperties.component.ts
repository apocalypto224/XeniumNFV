import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Server} from '../../servers/server.model';
import {DeleteVnfPortProperties} from './deleteVnfPortProperties.model';

const uuidv4 = require('uuid/v4');
@Component({
    selector: "Xnet-execDeleteVnfPortPropertiesComponent",
    styles: [],
    templateUrl: "./execDeleteVnfPortProperties.component.html"
})
export class ExecDeleteVnfPortPropertiesComponent implements OnInit{
    @Input() deleteVnfPortProperties:DeleteVnfPortProperties ;
    @Input() servers: Server[];
    @Output() applyChange = new EventEmitter();

    constructor() {}

    ngOnInit(){

    }

    emitApplyChange() {
        this.applyChange.emit(this.deleteVnfPortProperties);
    }
}
