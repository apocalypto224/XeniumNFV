export class NotifierVnfProperties {
    constructor(public topicName?: string,
                public containerName?: string,
                public metric?: string,
                public interfaceName?: string) {
        // CAT -> cAdvisor Topic
        this.topicName = topicName ? topicName : 'CAT';
        this.containerName = containerName ? containerName : '';
        this.metric = metric ? metric : 'CPU';
        this.interfaceName = interfaceName ? interfaceName : '';
    }
}
