import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NotifierVnfProperties} from './notifierVnfProperties.model';

@Component({
    selector: "Xnet-notifierVnfPropertiesComponent",
    styles: [],
    templateUrl: "./notifierVnfProperties.component.html"
})
export class NotifierVnfPropertiesComponent {
    @Input()
    notifierVnfProperties: NotifierVnfProperties;
    @Output() applyChange = new EventEmitter();


    constructor() {}

    emitApplyChange() {
        this.applyChange.emit(this.notifierVnfProperties);
    }
}
