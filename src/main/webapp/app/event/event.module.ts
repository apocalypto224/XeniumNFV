import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedCommonDirectiveModule} from '../shared/shared-common-directive.module';
import {EventComponent} from './event.component';
import {EventService} from './event.service';
import {EventRoute} from './event.routing';
import {EventEditComponent} from './event-edit.component';
import {NotifierExternalPropertiesComponent} from './notifierExternalProperties/notifierExternalProperties.component';
import {ExecCreateBridgePropertiesComponent} from './execCreateBridgeProperties/execCreateBridgeProperties.component';
import {ExecCreateVnfPropertiesComponent} from './execCreateVnfProperties/execCreateVnfProperties.component';
import {CreateVnfPortPropertiesComponent} from './createVnfPortProperties/createVnfPortProperties.component';
import {CreateBridgePatchPeerPortPropertiesComponent} from './createBridgePatchPeerPortProperties/createBridgePatchPeerPortProperties.component';
import {UpdateVnfPropertiesComponent} from './updateVnfProperties/updateVnfProperties.component';
import {MathStaticPropertiesComponent} from './mathStaticProperties/mathStaticProperties.component';
import {ExecDeleteBridgePropertiesComponent} from './execDeleteBridgeProperties/execDeleteBridgeProperties.component';
import {ExecDeleteVnfPropertiesComponent} from './execDeleteVnfProperties/execDeleteVnfProperties.component';
import {ExecDeleteBridgePatchPeerPortPropertiesComponent} from './execDeleteBridgePatchPeerPortProperties/execDeleteBridgePatchPeerPortProperties.component';
import {ExecDeleteVnfPortPropertiesComponent} from './execDeleteVnfPortProperties/execDeleteVnfPortProperties.component';
import {MathComparePropertiesComponent} from './mathCompareProperties/mathCompareProperties.component';
import {NotifierVnfPropertiesComponent} from './notifierVnfProperties/notifierVnfProperties.component';


@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        ReactiveFormsModule,
        SharedCommonDirectiveModule,
        EventRoute,
    ],
    declarations: [
        EventComponent,
        EventEditComponent,
        NotifierExternalPropertiesComponent,
        ExecCreateBridgePropertiesComponent,
        ExecCreateVnfPropertiesComponent,
        CreateBridgePatchPeerPortPropertiesComponent,
        CreateVnfPortPropertiesComponent,
        UpdateVnfPropertiesComponent,
        MathStaticPropertiesComponent,
        ExecDeleteBridgePropertiesComponent,
        ExecDeleteVnfPropertiesComponent,
        ExecDeleteBridgePatchPeerPortPropertiesComponent,
        ExecDeleteVnfPortPropertiesComponent,
        MathComparePropertiesComponent,
        NotifierVnfPropertiesComponent
    ],
    providers: [EventService]
})
export class EventModule {}
