import {AfterViewInit, Component, OnInit} from '@angular/core';
import {NotificationService} from '../shared/utils/notification.service';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {NetworkDiagramService} from '../networkDiagram/networkDiagram.service';
import * as go from 'gojs';
import {EventDiagramParserUtil} from './eventDiagramParser.util';
import {Event} from './event.model';
import {EventManager, ParseLinks} from 'ng-jhipster';
import {EventService} from './event.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NotifierExternalProperties} from './notifierExternalProperties/notifierExternalProperties.model';
import {Switch} from '../networkDiagram/accessory-switch/swicth.model';
import {Vnf} from '../networkDiagram/accessory-vnfs/vnf.model';
import {Server} from '../servers/server.model';
import {ServerService} from '../servers/server.service';
import {ResponseWrapper} from '../shared/model/response-wrapper.model';
import {CreateBridgePatchPeerPortProperties} from './createBridgePatchPeerPortProperties/createBridgePatchPeerPortProperties.model';
import {CreateVnfPortPropertiesComponent} from './createVnfPortProperties/createVnfPortProperties.component';
import {CreateVnfPortProperties} from './createVnfPortProperties/createVnfPortProperties.model';
import {UpdateVnfProperties} from './updateVnfProperties/updateVnfProperties.model';
import {MathStaticProperties} from './mathStaticProperties/mathStaticProperties.model';
import {DeleteBridgeProperties} from './execDeleteBridgeProperties/deleteBridgeProperties.model';
import {DeleteVnfProperties} from './execDeleteVnfProperties/deleteVnfProperties.model';
import {DeleteBridgePatchPeerPortProperties} from './execDeleteBridgePatchPeerPortProperties/deleteBridgePatchPeerPortProperties.model';
import {DeleteVnfPortProperties} from './execDeleteVnfPortProperties/deleteVnfPortProperties.model';
import {MathCompareProperties} from './mathCompareProperties/mathCompareProperties.model';
import {NodePort} from '../networkDiagram/accessory-vnfs/nodePort.model';
import {NotifierVnfProperties} from './notifierVnfProperties/notifierVnfProperties.model';

const $$ = go.GraphObject.make;
const uuidv4 = require('uuid/v4');
const FileSaver = require('file-saver');

@Component({
    selector: 'accessoryVnf-popup',
    templateUrl: './event-edit.component.html'
})
export class EventEditComponent extends go.Link implements OnInit, AfterViewInit {

    public myDiagram: any;
    public __this = this;
    private selectedPart: any;
    private selectedSfcId;
    private selectedEntytType: string = '';
    private tabSpinnerText = '';
    private eventObj: Event = new Event();
    /*
    pagination variable
     */
    error: any;
    success: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    itemsPerPage: any;
    /*
    Variable for properties
     */
    private notifierExternalProperties: NotifierExternalProperties = new NotifierExternalProperties();
    private notifierVnfProperties: NotifierVnfProperties = new NotifierVnfProperties();
    private swch: Switch = new Switch();
    private vnf: Vnf = new Vnf();
    public servers: Server[];
    private createBridgePatchPeerProperties: CreateBridgePatchPeerPortProperties = new CreateBridgePatchPeerPortProperties();
    private createVnfPortProperties: CreateVnfPortProperties = new CreateVnfPortProperties();
    private updateVnfProperties: UpdateVnfProperties = new UpdateVnfProperties();
    private mathStaticProperties: MathStaticProperties = new MathStaticProperties();
    private deleteBridgeProperties: DeleteBridgeProperties = new DeleteBridgeProperties();
    private deleteVnfProperties: DeleteVnfProperties = new DeleteVnfProperties();
    private deleteBridgePatchPeerPortProperties: DeleteBridgePatchPeerPortProperties = new DeleteBridgePatchPeerPortProperties();
    private deleteVnfPortProperties: DeleteVnfPortProperties = new DeleteVnfPortProperties();
    private mathCompareProperties: MathCompareProperties = new MathCompareProperties();

    constructor(private networkDiagramService: NetworkDiagramService,
                private notificationService: NotificationService,
                private eventManager: EventManager,
                private eventService: EventService,
                private router: Router,
                private parseLinks: ParseLinks,
                private serverService: ServerService,
                private ng4LoadingSpinnerService: Ng4LoadingSpinnerService,
                private activatedRoute: ActivatedRoute) {
        super();
        this.itemsPerPage = 100;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.loadAll();
    }

    ngOnInit() {
    }

    loadAll() {
        this.serverService
            .query('/getAll', {
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()
            })
            .subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(servers, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.servers = servers;
    }

    private onError(error) {
    }

    ngAfterViewInit() {
        ///////////////////////////////////////////////////////

        //for conciseness in defining node templates

        this.myDiagram = $$(
            go.Diagram,
            // this.eventEditDiagram.nativeElement.id, //Diagram refers to its DIV HTML element by id
            document.getElementById('eventEditDiagram'),
            {
                mouseDrop: (e) => {
                    this.finishDrop(e, null);
                },
                initialContentAlignment: go.Spot.Center,
                'undoManager.isEnabled': true,
                'commandHandler.archetypeGroupData': {text: 'SFC', isGroup: true, category: 'OfNodes'},
                allowDrop: false
            }
        );
        console.log(this.myDiagram);

        this.myDiagram.addDiagramListener('ObjectSingleClicked', e => {
            this.selectedEntytType = '';
            var part = e.subject.part;
            if (!(part instanceof go.Link)) {
                console.log('my part IS');
                console.log(part.data);
                if (part.data.isGroup === 'true' || part.data.isGroup == true) {
                    this.selectedSfcId = part.data.key;
                }
                this.selectedPart = part;
                if (part.data.name == 'NotifierExt') {
                    this.selectedEntytType = 'NotifierExt';
                    this.notifierExternalProperties.topicName = part.data.topicName;
                    this.notifierExternalProperties.valueType = part.data.valueType;
                    this.notifierExternalProperties.isLatch = part.data.isLatch === '1';
                }
                else if (part.data.name == 'NotifierSwitch') {
                    this.selectedEntytType = 'NotifierSwitch';
                }
                else if (part.data.name == 'NotifierVnf') {
                    this.selectedEntytType = 'NotifierVnf';
                    this.notifierVnfProperties.topicName = part.data.topicName;
                    this.notifierVnfProperties.containerName = part.data.containerName;
                    this.notifierVnfProperties.metric = part.data.metric;
                    this.notifierVnfProperties.interfaceName = part.data.interfaceName;
                }
                else if (part.data.name == 'StaticVal') {
                    this.selectedEntytType = 'StaticVal';
                    this.mathStaticProperties.value = part.data.value;
                    this.mathStaticProperties.valueType = part.data.valueType;
                }
                else if (part.data.name == 'Compare') {
                    this.selectedEntytType = 'Compare';
                    this.mathCompareProperties.operation = part.data.operation;
                }
                else if (part.data.name == 'CreateSwitch') {
                    this.selectedEntytType = 'CreateSwitch';
                    this.swch = part.data.swch;
                }
                else if (part.data.name == 'CreateVNF') {
                    this.selectedEntytType = 'CreateVNF';
                    this.vnf = part.data.vnf;
                }
                else if (part.data.name == 'CreatePatchPeer') {
                    this.selectedEntytType = 'CreatePatchPeer';
                    this.createBridgePatchPeerProperties.serverId = part.data.nodeId;
                    this.createBridgePatchPeerProperties.bridgeName1 = part.data.bridgeName1;
                    this.createBridgePatchPeerProperties.bridgeName2 = part.data.bridgeName2;
                }
                else if (part.data.name == 'CreateVNFPort') {
                    this.selectedEntytType = 'CreateVNFPort';
                    this.createVnfPortProperties.serverId = part.data.nodeId;
                    this.createVnfPortProperties.bridgeName = part.data.bridgeName;
                    this.createVnfPortProperties.containerName = part.data.containerName;
                    this.createVnfPortProperties.interfaceName = part.data.interfaceName;
                    this.createVnfPortProperties.ipAddress = part.data.ipAddress;
                    this.createVnfPortProperties.gateway = part.data.gateway;
                }
                else if (part.data.name == 'CreateODL') {
                    this.selectedEntytType = 'CreateODL';
                }
                else if (part.data.name == 'UpdateVNF') {
                    this.selectedEntytType = '';
                    setTimeout(() => {
                        this.selectedEntytType = 'UpdateVNF';
                    }, 100);

                    this.updateVnfProperties.serverId = part.data.nodeId;
                    this.updateVnfProperties.containerName = part.data.containerName;
                    this.updateVnfProperties.cpu = part.data.cpu;
                    this.updateVnfProperties.ram = part.data.ram;
                }
                else if (part.data.name == 'DeleteSwitch') {
                    this.selectedEntytType = 'DeleteSwitch';
                    this.deleteBridgeProperties.serverId = part.data.nodeId;
                    this.deleteBridgeProperties.bridgeName = part.data.bridgeName;
                }
                else if (part.data.name == 'DeleteVNF') {
                    this.selectedEntytType = 'DeleteVNF';
                    this.deleteVnfProperties.serverId = part.data.nodeId;
                    this.deleteVnfProperties.vnfName = part.data.vnfName;

                }
                else if (part.data.name == 'DeletePatchPeer') {
                    this.selectedEntytType = 'DeletePatchPeer';
                    this.deleteBridgePatchPeerPortProperties.serverId = part.data.nodeId;
                    this.deleteBridgePatchPeerPortProperties.bridgeName1 = part.data.bridgeName1;
                    this.deleteBridgePatchPeerPortProperties.bridgeName2 = part.data.bridgeName2;
                }
                else if (part.data.name == 'DeleteVNFPort') {
                    this.selectedEntytType = 'DeleteVNFPort';
                    this.deleteVnfPortProperties.serverId = part.data.nodeId;
                    this.deleteVnfPortProperties.containerName = part.data.containerName;
                    this.deleteVnfPortProperties.bridgeName = part.data.bridgeName;
                    this.deleteVnfPortProperties.interfaceName = part.data.interfaceName;
                }
                else if (part.data.name == 'DeleteODL') {
                    this.selectedEntytType = 'DeleteODL';
                }
            }
        });

        this.myDiagram.addDiagramListener('BackgroundSingleClicked', e => {
            this.selectedEntytType = ''
        });

        var nodeMenu = $$(
            // context menu for each Node
            go.Adornment,
            'Vertical',
            this.makeButton('Copy', (e, obj) => {
                e.diagram.commandHandler.copySelection();
            }),
            this.makeButton('Delete', (e, obj) => {
                e.diagram.commandHandler.deleteSelection();
            }),
            $$(go.Shape, 'LineH', {
                strokeWidth: 2,
                height: 1,
                stretch: go.GraphObject.Horizontal
            }),
            this.makeButton('Add top port', (e, obj) => {
                this.addPort('top');
            }),
            this.makeButton('Add left port', (e, obj) => {
                this.addPort('left');
            }),
            this.makeButton('Add right port', (e, obj) => {
                this.addPort('right');
            }),
            this.makeButton('Add bottom port', (e, obj) => {
                this.addPort('bottom');
            })
        );

        var portSize = new go.Size(8, 8);

        var portMenu = $$(
            // context menu for each port
            go.Adornment,
            'Vertical',
            this.makeButton(
                'Remove port',
                // in the click event handler, the obj.part is the Adornment;
                // its adornedObject is the port
                function(e, obj) {
                    this.removePort(obj.part.adornedObject);
                }
            ),
            this.makeButton('Change color', (e, obj) => {
                this.changeColor(obj.part.adornedObject);
            }),
            this.makeButton('Remove side ports', (e, obj) => {
                this.removeAll(obj.part.adornedObject);
            })
        );

        // Notifier Switch Node
        // ******************************************************************************************************************************
        let graygrad = $$(go.Brush, 'Linear',
            {0: '#3ab598', 0.5: '#16A085', 1: '#48f3d9'}); // sabz

        this.myDiagram.nodeTemplateMap.add('NotifierVNF',
            $$(go.Node, 'Table',
                {
                    locationObjectName: 'BODY',
                    locationSpot: go.Spot.Center,
                    selectionObjectName: 'BODY',
                    contextMenu: nodeMenu
                },
                new go.Binding('location', 'loc', go.Point.parse).makeTwoWay(go.Point.stringify),

                // the body
                $$(go.Panel, 'Auto',
                    {
                        row: 1, column: 1, name: 'BODY',
                        stretch: go.GraphObject.Fill,

                    },
                    $$(go.Shape, 'RoundedRectangle',
                        {
                            fill: graygrad, stroke: 'black', strokeWidth: 0,
                            minSize: new go.Size(80, 100)
                        }),
                    // $$(go.TextBlock,
                    //     {
                    //         row: 0, columnSpan: 2, margin: 3, alignment: go.Spot.Center,
                    //         font: 'bold 10pt sans-serif',
                    //         isMultiline: false, editable: false,
                    //         textAlign: 'center',
                    //         width: 65,
                    //         height: 90,
                    //         verticalAlignment: go.Spot.Top
                    //     },
                    //     new go.Binding('text', 'name').makeTwoWay()),
                    $$(go.TextBlock,
                        {
                            margin: 10,
                            textAlign: 'center',
                            font: '14px  Segoe UI,sans-serif',
                            stroke: 'white', editable: false
                        },
                        new go.Binding('text', 'name').makeTwoWay())
                ),  // end Auto Panel body

                // the Panel holding the left port elements, which are themselves Panels,
                // created for each item in the itemArray, bound to data.leftArray
                $$(go.Panel, 'Vertical',
                    new go.Binding('itemArray', 'leftArray'),
                    {
                        row: 1, column: 0,
                        itemTemplate:
                            $$(go.Panel,
                                {
                                    _side: 'left',  // internal property to make it easier to tell which side it's on
                                    fromSpot: go.Spot.Left, toSpot: go.Spot.Left,
                                    fromLinkable: false, toLinkable: true, cursor: 'pointer', fromLinkableDuplicates: false, toLinkableDuplicates: false,
                                    fromMaxLinks: 1,
                                    toMaxLinks: 1,
                                    contextMenu: portMenu
                                },
                                new go.Binding('portId', 'portId'),
                                $$(go.Shape, 'PrimitiveToCall',
                                    {
                                        stroke: 'black', strokeWidth: 1,
                                        desiredSize: portSize,
                                        margin: new go.Margin(5, 0)
                                    },
                                    new go.Binding('fill', 'portColor'))
                            )  // end itemTemplate
                    }
                ),  // end Vertical Panel


                // the Panel holding the right port elements, which are themselves Panels,
                // created for each item in the itemArray, bound to data.rightArray
                $$(go.Panel, 'Vertical',
                    new go.Binding('itemArray', 'rightArray'),
                    {
                        row: 1, column: 2,
                        itemTemplate:
                            $$(go.Panel,
                                {
                                    _side: 'right',
                                    fromSpot: go.Spot.Right, toSpot: go.Spot.Right,
                                    fromLinkable: true, toLinkable: false, cursor: 'pointer', fromLinkableDuplicates: false, toLinkableDuplicates: false,
                                    contextMenu: portMenu
                                },
                                new go.Binding('portId', 'portId'),
                                $$(go.Shape, 'PrimitiveToCall',
                                    {
                                        stroke: 'black', strokeWidth: 1,

                                        desiredSize: portSize,
                                        margin: new go.Margin(1, 0)
                                    },
                                    new go.Binding('fill', 'portColor'))
                            )  // end itemTemplate
                    }
                ),  // end Vertical Panel

            ));  // end Node

        // Notifier Switch Node
        // ******************************************************************************************************************************
        this.myDiagram.nodeTemplateMap.add('NotifierSwitch',
            $$(go.Node, 'Table',
                {
                    locationObjectName: 'BODY',
                    locationSpot: go.Spot.Center,
                    selectionObjectName: 'BODY',
                    contextMenu: nodeMenu
                },
                new go.Binding('location', 'loc', go.Point.parse).makeTwoWay(go.Point.stringify),

                // the body
                $$(go.Panel, 'Auto',
                    {
                        row: 1, column: 1, name: 'BODY',
                        stretch: go.GraphObject.Fill,

                    },
                    $$(go.Shape, 'RoundedRectangle',
                        {
                            fill: graygrad, stroke: 'black', strokeWidth: 0,
                            minSize: new go.Size(80, 100)
                        }),
                    // $$(go.TextBlock,
                    //     {
                    //         row: 0, columnSpan: 2, margin: 3, alignment: go.Spot.Center,
                    //         font: 'bold 12pt sans-serif',
                    //         isMultiline: false, editable: false,
                    //         textAlign: 'center',
                    //         width: 65,
                    //         height: 90,
                    //         verticalAlignment: go.Spot.Top,
                    //     },
                    //     new go.Binding('text', 'name').makeTwoWay()),
                    $$(go.TextBlock,
                        {margin: 10, textAlign: 'center', font: '14px  Segoe UI,sans-serif', stroke: 'white', editable: true},
                        new go.Binding('text', 'name').makeTwoWay())
                ),  // end Auto Panel body

                // the Panel holding the left port elements, which are themselves Panels,
                // created for each item in the itemArray, bound to data.leftArray
                $$(go.Panel, 'Vertical',
                    new go.Binding('itemArray', 'leftArray'),
                    {
                        row: 1, column: 0,
                        itemTemplate:
                            $$(go.Panel,
                                {
                                    _side: 'left',  // internal property to make it easier to tell which side it's on
                                    fromSpot: go.Spot.Left, toSpot: go.Spot.Left,
                                    fromLinkable: false, toLinkable: true, cursor: 'pointer', fromLinkableDuplicates: false, toLinkableDuplicates: false,
                                    fromMaxLinks: 1,
                                    toMaxLinks: 1,
                                    contextMenu: portMenu
                                },
                                new go.Binding('portId', 'portId'),
                                $$(go.Shape, 'PrimitiveToCall',
                                    {
                                        stroke: 'black', strokeWidth: 1,
                                        desiredSize: portSize,
                                        margin: new go.Margin(5, 0)
                                    },
                                    new go.Binding('fill', 'portColor'))
                            )  // end itemTemplate
                    }
                ),  // end Vertical Panel


                // the Panel holding the right port elements, which are themselves Panels,
                // created for each item in the itemArray, bound to data.rightArray
                $$(go.Panel, 'Vertical',
                    new go.Binding('itemArray', 'rightArray'),
                    {
                        row: 1, column: 2,
                        itemTemplate:
                            $$(go.Panel,
                                {
                                    _side: 'right',
                                    fromSpot: go.Spot.Right, toSpot: go.Spot.Right,
                                    fromLinkable: true, toLinkable: false, cursor: 'pointer', fromLinkableDuplicates: false, toLinkableDuplicates: false,
                                    contextMenu: portMenu
                                },
                                new go.Binding('portId', 'portId'),
                                $$(go.Shape, 'PrimitiveToCall',
                                    {
                                        stroke: 'black', strokeWidth: 1,
                                        desiredSize: portSize,
                                        margin: new go.Margin(1, 0)
                                    },
                                    new go.Binding('fill', 'portColor'))
                            )  // end itemTemplate
                    }
                ),  // end Vertical Panel

            ));  // end Node


        // Notifier External Node
        // ******************************************************************************************************************************

        this.myDiagram.nodeTemplateMap.add('NotifierExternal',
            $$(go.Node, 'Table',
                {
                    locationObjectName: 'BODY',
                    locationSpot: go.Spot.Center,
                    selectionObjectName: 'BODY',
                    contextMenu: nodeMenu
                },
                new go.Binding('location', 'loc', go.Point.parse).makeTwoWay(go.Point.stringify),

                // the body
                $$(go.Panel, 'Auto',
                    {
                        row: 1, column: 1, name: 'BODY',
                        stretch: go.GraphObject.Fill,

                    },
                    $$(go.Shape, 'RoundedRectangle',
                        {
                            fill: graygrad, stroke: 'black', strokeWidth: 0,
                            minSize: new go.Size(80, 100)
                        }),
                    // $$(go.TextBlock,
                    //     {
                    //         row: 0, columnSpan: 2, margin: 3, alignment: go.Spot.Center,
                    //         font: 'bold 12pt sans-serif',
                    //         isMultiline: false, editable: false,
                    //         textAlign: 'center',
                    //         width: 65,
                    //         height: 90,
                    //         verticalAlignment: go.Spot.Top,
                    //     },
                    //     new go.Binding('text', 'name').makeTwoWay()),
                    $$(go.TextBlock,
                        {margin: 10, textAlign: 'center', font: '14px  Segoe UI,sans-serif', stroke: 'white', editable: true},
                        new go.Binding('text', 'name').makeTwoWay())
                ),  // end Auto Panel body

                // the Panel holding the left port elements, which are themselves Panels,
                // created for each item in the itemArray, bound to data.leftArray
                $$(go.Panel, 'Vertical',
                    new go.Binding('itemArray', 'leftArray'),
                    {
                        row: 1, column: 0,
                        itemTemplate:
                            $$(go.Panel,
                                {
                                    _side: 'left',  // internal property to make it easier to tell which side it's on
                                    fromSpot: go.Spot.Left, toSpot: go.Spot.Left,
                                    fromLinkable: false, toLinkable: true, cursor: 'pointer', fromLinkableDuplicates: false, toLinkableDuplicates: false,
                                    fromMaxLinks: 1,
                                    toMaxLinks: 1,
                                    contextMenu: portMenu
                                },
                                new go.Binding('portId', 'portId'),
                                $$(go.Shape, 'PrimitiveToCall',
                                    {
                                        stroke: 'black', strokeWidth: 1,
                                        desiredSize: portSize,
                                        margin: new go.Margin(5, 0)
                                    },
                                    new go.Binding('fill', 'portColor'))
                            )  // end itemTemplate
                    }
                ),  // end Vertical Panel


                // the Panel holding the right port elements, which are themselves Panels,
                // created for each item in the itemArray, bound to data.rightArray
                $$(go.Panel, 'Vertical',
                    new go.Binding('itemArray', 'rightArray'),
                    {
                        row: 1, column: 2,
                        itemTemplate:
                            $$(go.Panel,
                                {
                                    _side: 'right',
                                    fromSpot: go.Spot.Right, toSpot: go.Spot.Right,
                                    fromLinkable: true, toLinkable: false, cursor: 'pointer', fromLinkableDuplicates: false, toLinkableDuplicates: false,
                                    contextMenu: portMenu
                                },
                                new go.Binding('portId', 'portId'),
                                $$(go.Shape, 'PrimitiveToCall',
                                    {
                                        stroke: 'black', strokeWidth: 1,

                                        desiredSize: portSize,
                                        margin: new go.Margin(1, 0)
                                    },
                                    new go.Binding('fill', 'portColor'))
                            )  // end itemTemplate
                    }
                ),  // end Vertical Panel

            ));  // end Node

        // Static Node
        // ******************************************************************************************************************************
        let grgrad = $$(go.Brush, 'Linear',
            {0: '#01af0e', 0.5: '#2d7c26', 1: '#86e288'}); // Abi

        this.myDiagram.nodeTemplateMap.add('Static',
            $$(go.Node, 'Table',
                {
                    locationObjectName: 'BODY',
                    locationSpot: go.Spot.Center,
                    selectionObjectName: 'BODY',
                    contextMenu: nodeMenu
                },
                new go.Binding('location', 'loc', go.Point.parse).makeTwoWay(go.Point.stringify),

                // the body
                $$(go.Panel, 'Auto',
                    {
                        row: 1, column: 1, name: 'BODY',
                        stretch: go.GraphObject.Fill,

                    },
                    $$(go.Shape, 'RoundedRectangle',
                        {
                            fill: grgrad, stroke: 'black', strokeWidth: 0,
                            minSize: new go.Size(50, 50)
                        }),
                    $$(go.TextBlock,
                        {margin: 10, textAlign: 'center', font: '14px  Segoe UI,sans-serif', stroke: 'white', editable: true},
                        new go.Binding('text', 'name').makeTwoWay()),
                    $$(go.TextBlock,
                        {
                            row: 0, columnSpan: 2, margin: 3, alignment: go.Spot.Center,
                            font: '12px Segoe UI,sans-serif',
                            stroke: 'white',
                            isMultiline: false, editable: false,
                            textAlign: 'center',
                            width: 65,
                            height: 50,
                            verticalAlignment: go.Spot.Bottom,
                        },
                        new go.Binding('text', 'value').makeTwoWay()),
                ),  // end Auto Panel body

                // the Panel holding the left port elements, which are themselves Panels,
                // created for each item in the itemArray, bound to data.leftArray
                $$(go.Panel, 'Vertical',
                    new go.Binding('itemArray', 'leftArray'),
                    {
                        row: 1, column: 0,
                        itemTemplate:
                            $$(go.Panel,
                                {
                                    _side: 'left',  // internal property to make it easier to tell which side it's on
                                    fromSpot: go.Spot.Left, toSpot: go.Spot.Left,
                                    fromLinkable: false, toLinkable: true, cursor: 'pointer', fromLinkableDuplicates: false, toLinkableDuplicates: false,
                                    fromMaxLinks: 1,
                                    toMaxLinks: 1,
                                    contextMenu: portMenu
                                },
                                new go.Binding('portId', 'portId'),
                                $$(go.Shape, 'PrimitiveToCall',
                                    {
                                        stroke: 'black', strokeWidth: 1,
                                        desiredSize: portSize,
                                        margin: new go.Margin(5, 0)
                                    },
                                    new go.Binding('fill', 'portColor'))
                            )  // end itemTemplate
                    }
                ),  // end Vertical Panel


                // the Panel holding the right port elements, which are themselves Panels,
                // created for each item in the itemArray, bound to data.rightArray
                $$(go.Panel, 'Vertical',
                    new go.Binding('itemArray', 'rightArray'),
                    {
                        row: 1, column: 2,
                        itemTemplate:
                            $$(go.Panel,
                                {
                                    _side: 'right',
                                    fromSpot: go.Spot.Right, toSpot: go.Spot.Right,
                                    fromLinkable: true, toLinkable: false, cursor: 'pointer', fromLinkableDuplicates: false, toLinkableDuplicates: false,
                                    contextMenu: portMenu
                                },
                                new go.Binding('portId', 'portId'),
                                $$(go.Shape, 'PrimitiveToCall',
                                    {
                                        stroke: 'black', strokeWidth: 1,

                                        desiredSize: portSize,
                                        margin: new go.Margin(1, 0)
                                    },
                                    new go.Binding('fill', 'portColor'))
                            )  // end itemTemplate
                    }
                ),  // end Vertical Panel

            ));  // end Node

        // EXEC Node
        // ******************************************************************************************************************************
        let bluegrad = $$(go.Brush, 'Linear',
            {0: '#409CF3', 0.5: '#404CF3', 1: '#409CF3'}); // Abi

        this.myDiagram.nodeTemplateMap.add('Exec',
            $$(go.Node, 'Table',
                {
                    locationObjectName: 'BODY',
                    locationSpot: go.Spot.Center,
                    selectionObjectName: 'BODY',
                    contextMenu: nodeMenu
                },
                new go.Binding('location', 'loc', go.Point.parse).makeTwoWay(go.Point.stringify),

                // the body
                $$(go.Panel, 'Auto',
                    {
                        row: 1, column: 1, name: 'BODY',
                        stretch: go.GraphObject.Fill,

                    },
                    $$(go.Shape, 'RoundedRectangle',
                        {
                            fill: bluegrad, stroke: 'black', strokeWidth: 0,
                            minSize: new go.Size(80, 100)
                        }),
                    // $$(go.TextBlock,
                    //     {
                    //         row: 0, columnSpan: 2, margin: 3, alignment: go.Spot.Center,
                    //         font: 'bold 12pt sans-serif',
                    //         isMultiline: false, editable: false,
                    //         textAlign: 'center',
                    //         width: 65,
                    //         height: 90,
                    //         verticalAlignment: go.Spot.Top,
                    //     },
                    //     new go.Binding('text', 'name').makeTwoWay()),
                    $$(go.TextBlock,
                        {margin: 10, textAlign: 'center', font: '14px  Segoe UI,sans-serif', stroke: 'white', editable: true},
                        new go.Binding('text', 'name').makeTwoWay())
                ),  // end Auto Panel body

                // the Panel holding the left port elements, which are themselves Panels,
                // created for each item in the itemArray, bound to data.leftArray
                $$(go.Panel, 'Vertical',
                    new go.Binding('itemArray', 'leftArray'),
                    {
                        row: 1, column: 0,
                        itemTemplate:
                            $$(go.Panel,
                                {
                                    _side: 'left',  // internal property to make it easier to tell which side it's on
                                    fromSpot: go.Spot.Left, toSpot: go.Spot.Left,
                                    fromLinkable: false, toLinkable: true, cursor: 'pointer', fromLinkableDuplicates: false, toLinkableDuplicates: false,
                                    fromMaxLinks: 1,
                                    toMaxLinks: 1,
                                    contextMenu: portMenu
                                },
                                new go.Binding('portId', 'portId'),
                                $$(go.Shape, 'PrimitiveToCall',
                                    {
                                        stroke: 'black', strokeWidth: 1,
                                        desiredSize: portSize,
                                        margin: new go.Margin(5, 0)
                                    },
                                    new go.Binding('fill', 'portColor'))
                            )  // end itemTemplate
                    }
                ),  // end Vertical Panel


                // the Panel holding the right port elements, which are themselves Panels,
                // created for each item in the itemArray, bound to data.rightArray
                $$(go.Panel, 'Vertical',
                    new go.Binding('itemArray', 'rightArray'),
                    {
                        row: 1, column: 2,
                        itemTemplate:
                            $$(go.Panel,
                                {
                                    _side: 'right',
                                    fromSpot: go.Spot.Right, toSpot: go.Spot.Right,
                                    fromLinkable: true, toLinkable: false, cursor: 'pointer', fromLinkableDuplicates: false, toLinkableDuplicates: false,
                                    contextMenu: portMenu
                                },
                                new go.Binding('portId', 'portId'),
                                $$(go.Shape, 'PrimitiveToCall',
                                    {
                                        stroke: 'black', strokeWidth: 1,

                                        desiredSize: portSize,
                                        margin: new go.Margin(1, 0)
                                    },
                                    new go.Binding('fill', 'portColor'))
                            )  // end itemTemplate
                    }
                ),  // end Vertical Panel

            ));  // end Node

        // Logic Node
        // ******************************************************************************************************************************
        let pgrad = $$(go.Brush, 'Linear',
            {0: '#c122bf', 0.5: '#9D2BA7', 1: '#da7cd5'}); //banafsh

        this.myDiagram.nodeTemplateMap.add('Logic',
            $$(go.Node, 'Table',
                {
                    locationObjectName: 'BODY',
                    locationSpot: go.Spot.Center,
                    selectionObjectName: 'BODY',
                    contextMenu: nodeMenu
                },
                new go.Binding('location', 'loc', go.Point.parse).makeTwoWay(go.Point.stringify),

                // the body
                $$(go.Panel, 'Auto',
                    {
                        row: 1, column: 1, name: 'BODY',
                        stretch: go.GraphObject.Fill,

                    },
                    $$(go.Shape, 'RoundedRectangle',
                        {
                            fill: pgrad, stroke: 'black', strokeWidth: 0,
                            minSize: new go.Size(60, 50)
                        }),
                    // $$(go.TextBlock,
                    //     {
                    //         row: 0, columnSpan: 2, margin: 3, alignment: go.Spot.Center,
                    //         font: 'bold 12pt sans-serif',
                    //         isMultiline: false, editable: false,
                    //         textAlign: 'center',
                    //         width: 65,
                    //         height: 90,
                    //         verticalAlignment: go.Spot.Top,
                    //     },
                    //     new go.Binding('text', 'name').makeTwoWay()),
                    $$(go.TextBlock,
                        {margin: 10, textAlign: 'center', font: '14px  Segoe UI,sans-serif', stroke: 'white', editable: true},
                        new go.Binding('text', 'name').makeTwoWay())
                ),  // end Auto Panel body

                // the Panel holding the left port elements, which are themselves Panels,
                // created for each item in the itemArray, bound to data.leftArray
                $$(go.Panel, 'Vertical',
                    new go.Binding('itemArray', 'leftArray'),
                    {
                        row: 1, column: 0,
                        itemTemplate:
                            $$(go.Panel,
                                {
                                    _side: 'left',  // internal property to make it easier to tell which side it's on
                                    fromSpot: go.Spot.Left, toSpot: go.Spot.Left,
                                    fromLinkable: false, toLinkable: true, cursor: 'pointer', fromLinkableDuplicates: false, toLinkableDuplicates: false,
                                    fromMaxLinks: 1,
                                    toMaxLinks: 1,
                                    contextMenu: portMenu
                                },
                                new go.Binding('portId', 'portId'),
                                $$(go.Shape, 'PrimitiveToCall',
                                    {
                                        stroke: 'black', strokeWidth: 1,
                                        desiredSize: portSize,
                                        margin: new go.Margin(5, 0)
                                    },
                                    new go.Binding('fill', 'portColor'))
                            )  // end itemTemplate
                    }
                ),  // end Vertical Panel


                // the Panel holding the right port elements, which are themselves Panels,
                // created for each item in the itemArray, bound to data.rightArray
                $$(go.Panel, 'Vertical',
                    new go.Binding('itemArray', 'rightArray'),
                    {
                        row: 1, column: 2,
                        itemTemplate:
                            $$(go.Panel,
                                {
                                    _side: 'right',
                                    fromSpot: go.Spot.Right, toSpot: go.Spot.Right,
                                    fromLinkable: true, toLinkable: false, cursor: 'pointer', fromLinkableDuplicates: false, toLinkableDuplicates: false,
                                    contextMenu: portMenu
                                },
                                new go.Binding('portId', 'portId'),
                                $$(go.Shape, 'PrimitiveToCall',
                                    {
                                        stroke: 'black', strokeWidth: 1,

                                        desiredSize: portSize,
                                        margin: new go.Margin(1, 0)
                                    },
                                    new go.Binding('fill', 'portColor'))
                            )  // end itemTemplate
                    }
                ),  // end Vertical Panel

            ));  // end Node


        // Math Node
        // ******************************************************************************************************************************
        let yelgrad = $$(go.Brush, 'Linear',
            {0: '#bcaa19', 0.5: '#7c7b1e', 1: '#d8d981'}); // Abi

        this.myDiagram.nodeTemplateMap.add('Math',
            $$(go.Node, 'Table',
                {
                    locationObjectName: 'BODY',
                    locationSpot: go.Spot.Center,
                    selectionObjectName: 'BODY',
                    contextMenu: nodeMenu
                },
                new go.Binding('location', 'loc', go.Point.parse).makeTwoWay(go.Point.stringify),

                // the body
                $$(go.Panel, 'Auto',
                    {
                        row: 1, column: 1, name: 'BODY',
                        stretch: go.GraphObject.Fill,

                    },
                    $$(go.Shape, 'RoundedRectangle',
                        {
                            fill: yelgrad, stroke: 'black', strokeWidth: 0,
                            minSize: new go.Size(60, 50)
                        }),
                    // $$(go.TextBlock,
                    //     {
                    //         row: 0, columnSpan: 2, margin: 3, alignment: go.Spot.Center,
                    //         font: 'bold 12pt sans-serif',
                    //         isMultiline: false, editable: false,
                    //         textAlign: 'center',
                    //         width: 65,
                    //         height: 90,
                    //         verticalAlignment: go.Spot.Top,
                    //     },
                    //     new go.Binding('text', 'name').makeTwoWay()),
                    $$(go.TextBlock,
                        {margin: 10, textAlign: 'center', font: '14px  Segoe UI,sans-serif', stroke: 'white', editable: true},
                        new go.Binding('text', 'name').makeTwoWay()),
                    $$(go.TextBlock,
                        {
                            row: 0, columnSpan: 2, margin: 3, alignment: go.Spot.Center,
                            font: 'bold 12px Segoe UI,sans-serif',
                            stroke: 'white',
                            isMultiline: false, editable: false,
                            textAlign: 'center',
                            width: 60,
                            height: 50,
                            verticalAlignment: go.Spot.Bottom,
                        },
                        new go.Binding('text', 'operation').makeTwoWay()),
                ),  // end Auto Panel body

                // the Panel holding the left port elements, which are themselves Panels,
                // created for each item in the itemArray, bound to data.leftArray
                $$(go.Panel, 'Vertical',
                    new go.Binding('itemArray', 'leftArray'),
                    {
                        row: 1, column: 0,
                        itemTemplate:
                            $$(go.Panel,
                                {
                                    _side: 'left',  // internal property to make it easier to tell which side it's on
                                    fromSpot: go.Spot.Left, toSpot: go.Spot.Left,
                                    fromLinkable: false, toLinkable: true, cursor: 'pointer', fromLinkableDuplicates: false, toLinkableDuplicates: false,
                                    fromMaxLinks: 1,
                                    toMaxLinks: 1,
                                    contextMenu: portMenu
                                },
                                new go.Binding('portId', 'portId'),
                                $$(go.Shape, 'PrimitiveToCall',
                                    {
                                        stroke: 'black', strokeWidth: 1,
                                        desiredSize: portSize,
                                        margin: new go.Margin(5, 0)
                                    },
                                    new go.Binding('fill', 'portColor')),
                                $$(
                                    go.TextBlock,
                                    {
                                        margin: 1,
                                        verticalAlignment: go.Spot.Top,
                                        font: 'bold 4px  Segoe UI,sans-serif',
                                        stroke: 'black',
                                        // angle: "270",
                                        textAlign: 'left',
                                        width: 8,
                                        height: 15
                                    },
                                    new go.Binding('text', 'portId').makeTwoWay()
                                ),
                            )
                        // end itemTemplate
                    }
                ),  // end Vertical Panel


                // the Panel holding the right port elements, which are themselves Panels,
                // created for each item in the itemArray, bound to data.rightArray
                $$(go.Panel, 'Vertical',
                    new go.Binding('itemArray', 'rightArray'),
                    {
                        row: 1, column: 2,
                        itemTemplate:
                            $$(go.Panel,
                                {
                                    _side: 'right',
                                    fromSpot: go.Spot.Right, toSpot: go.Spot.Right,
                                    fromLinkable: true, toLinkable: false, cursor: 'pointer', fromLinkableDuplicates: false, toLinkableDuplicates: false,
                                    contextMenu: portMenu
                                },
                                new go.Binding('portId', 'portId'),
                                $$(go.Shape, 'PrimitiveToCall',
                                    {
                                        stroke: 'black', strokeWidth: 1,

                                        desiredSize: portSize,
                                        margin: new go.Margin(1, 0)
                                    },
                                    new go.Binding('fill', 'portColor'))
                            )  // end itemTemplate
                    }
                ),  // end Vertical Panel

            ));  // end Node

        // ******************************************************************************************************************************

        let CustomLink: any = function(): any {
            go.Link.call(this);
        };

        // an orthogonal link template, reshapable and relinkable
        this.myDiagram.linkTemplate =
            $$(go.Link,
                {
                    selectionAdornmentTemplate:
                        $$(go.Adornment,
                            $$(go.Shape,
                                {isPanelMain: true, stroke: 'dodgerblue', strokeWidth: 3}),
                            $$(go.Shape,
                                {toArrow: 'Standard', fill: 'dodgerblue', stroke: null, scale: 1})
                        ),
                    routing: go.Link.Normal,
                    curve: go.Link.Bezier,
                    toShortLength: 2
                },
                $$(go.Shape,  //  the link shape
                    {name: 'OBJSHAPE'}),
                $$(go.Shape,  // the arrowhead, at the mid point of the link
                    {toArrow: 'OpenTriangle', segmentIndex: -Infinity})
            );

        // support double-clicking in the background to add a copy of this data as a node
        this.myDiagram.toolManager.clickCreatingTool.archetypeNodeData = {
            name: 'Unit',
            leftArray: [],
            rightArray: [],
            topArray: [],
            bottomArray: []
        };

        this.myDiagram.contextMenu = $$(
            go.Adornment,
            'Vertical',
            this.makeButton(
                'Paste',
                function(e, obj) {
                    e.diagram.commandHandler.pasteSelection(
                        e.diagram.lastInput.documentPoint
                    );
                },
                function(o) {
                    return o.diagram.commandHandler.canPasteSelection();
                }
            ),
            this.makeButton(
                'Undo',
                function(e, obj) {
                    e.diagram.commandHandler.undo();
                },
                function(o) {
                    return o.diagram.commandHandler.canUndo();
                }
            ),
            this.makeButton(
                'Redo',
                function(e, obj) {
                    e.diagram.commandHandler.redo();
                },
                function(o) {
                    return o.diagram.commandHandler.canRedo();
                }
            )
        );

        // load the diagram from JSON data
        this.load();

        go.Diagram.inherit(CustomLink, go.Link);

        CustomLink.prototype.findSidePortIndexAndCount = function(node, port) {
            var nodedata = node.data;
            if (nodedata !== null) {
                var portdata = port.data;
                var side = port._side;
                var arr = nodedata[side + 'Array'];
                var len = arr.length;
                for (var i = 0; i < len; i++) {
                    if (arr[i] === portdata) return [i, len];
                }
            }
            return [-1, len];
        };

        /** @override */
        CustomLink.prototype.computeEndSegmentLength = function(node, port, spot, from) {
            // var esl = go.Link.prototype.computeEndSegmentLength.call(this, node, port, spot, from);
            var esl = this.__this.computeEndSegmentLength.call(
                this,
                node,
                port,
                spot,
                from
            );
            var other = this.getOtherPort(port);
            if (port !== null && other !== null) {
                var thispt = port.getDocumentPoint(this.computeSpot(from));
                var otherpt = other.getDocumentPoint(this.computeSpot(!from));
                if (
                    Math.abs(thispt.x - otherpt.x) > 20 ||
                    Math.abs(thispt.y - otherpt.y) > 20
                ) {
                    var info = this.findSidePortIndexAndCount(node, port);
                    var idx = info[0];
                    var count = info[1];
                    if (port._side == 'top' || port._side == 'bottom') {
                        if (otherpt.x < thispt.x) {
                            return esl + 4 + idx * 8;
                        } else {
                            return esl + (count - idx - 1) * 8;
                        }
                    } else {
                        // left or right
                        if (otherpt.y < thispt.y) {
                            return esl + 4 + idx * 8;
                        } else {
                            return esl + (count - idx - 1) * 8;
                        }
                    }
                }
            }
            return esl;
        };

        /** @override */
        CustomLink.prototype.hasCurviness = function() {
            if (isNaN(this.curviness)) return true;
            return this.__this.hasCurviness.call(this);
        };

        /** @override */
        CustomLink.prototype.computeCurviness = function() {
            if (isNaN(this.curviness)) {
                var fromnode = this.fromNode;
                var fromport = this.fromPort;
                var fromspot = this.computeSpot(true);
                var frompt = fromport.getDocumentPoint(fromspot);
                var tonode = this.toNode;
                var toport = this.toPort;
                var tospot = this.computeSpot(false);
                var topt = toport.getDocumentPoint(tospot);
                if (
                    Math.abs(frompt.x - topt.x) > 20 ||
                    Math.abs(frompt.y - topt.y) > 20
                ) {
                    if (
                        (fromspot.equals(go.Spot.Left) ||
                            fromspot.equals(go.Spot.Right)) &&
                        (tospot.equals(go.Spot.Left) ||
                            tospot.equals(go.Spot.Right))
                    ) {
                        var fromseglen = this.computeEndSegmentLength(
                            fromnode,
                            fromport,
                            fromspot,
                            true
                        );
                        var toseglen = this.computeEndSegmentLength(
                            tonode,
                            toport,
                            tospot,
                            false
                        );
                        var c = (fromseglen - toseglen) / 2;
                        if (frompt.x + fromseglen >= topt.x - toseglen) {
                            if (frompt.y < topt.y) return c;
                            if (frompt.y > topt.y) return -c;
                        }
                    } else if (
                        (fromspot.equals(go.Spot.Top) ||
                            fromspot.equals(go.Spot.Bottom)) &&
                        (tospot.equals(go.Spot.Top) ||
                            tospot.equals(go.Spot.Bottom))
                    ) {
                        var fromseglen = this.computeEndSegmentLength(
                            fromnode,
                            fromport,
                            fromspot,
                            true
                        );
                        var toseglen = this.computeEndSegmentLength(
                            tonode,
                            toport,
                            tospot,
                            false
                        );
                        var c = (fromseglen - toseglen) / 2;
                        if (frompt.x + fromseglen >= topt.x - toseglen) {
                            if (frompt.y < topt.y) return c;
                            if (frompt.y > topt.y) return -c;
                        }
                    }
                }
            }
            return this.__this.computeCurviness.call(this);
        };

        ////////////////////////////////////////////////////////
    }

    createSwitchNotifier() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'NotifierSwitch#' + rand,
                name: 'NotifierSwitch',
                describtion: 'NotifierSwitch',
                loc: '0 0',
                category: 'NotifierSwitch',
                value: '',
                valueType: '',
                delayNum: '',
                leftArray: [],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createVNFNotifier() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'NotifierVnf#' + rand,
                name: 'NotifierVnf',
                describtion: 'NotifierVnf',
                loc: '0 0',
                category: 'NotifierVNF',
                value: '',
                valueType: 'INTEGER',
                delayNum: '',
                topicName: 'T_VNF',
                containerName: '',
                metric: 'CPU',
                interfaceName: '',
                leftArray: [],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createExternalNotifier() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'NotifierExt#' + rand,
                name: 'NotifierExt',
                describtion: 'NotifierExt',
                loc: '0 0',
                category: 'NotifierExternal',
                value: '',
                valueType: 'BOOLEAN',
                delayNum: '',
                topicName: '',
                isLatch: '',
                leftArray: [],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createLogicActionAND() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'AND#' + rand,
                name: 'AND',
                describtion: 'AND',
                loc: '0 0',
                category: 'Logic',
                value: '',
                valueType: '',
                delayNum: '',
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'left0', 'value': '', 'valueType': '', 'delayNum': ''}, {
                    'portColor': '#FFFFFF',
                    'portId': 'left1',
                    'value': '',
                    'valueType': '',
                    'delayNum': ''
                }],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createLogicActionOR() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'OR#' + rand,
                name: 'OR',
                describtion: 'OR',
                loc: '0 0',
                category: 'Logic',
                value: '',
                valueType: '',
                delayNum: '',
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'left0', 'value': '', 'valueType': '', 'delayNum': ''}, {
                    'portColor': '#FFFFFF',
                    'portId': 'left1',
                    'value': '',
                    'valueType': '',
                    'delayNum': ''
                }],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createStaticVal() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'StaticVal#' + rand,
                name: 'StaticVal',
                describtion: 'StaticVal',
                loc: '0 0',
                category: 'Static',
                value: '',
                valueType: '',
                delayNum: '',
                leftArray: [],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createMathActionADD() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'ADD#' + rand,
                name: 'ADD',
                describtion: 'ADD',
                loc: '0 0',
                category: 'Math',
                value: '',
                valueType: '',
                delayNum: '',
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'A', 'value': '', 'valueType': '', 'delayNum': ''}, {
                    'portColor': '#FFFFFF',
                    'portId': 'B',
                    'value': '',
                    'valueType': '',
                    'delayNum': ''
                }],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createMathActionSUB() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'SUB#' + rand,
                name: 'SUB',
                describtion: 'SUB',
                loc: '0 0',
                category: 'Math',
                value: '',
                valueType: '',
                delayNum: '',
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'A', 'value': '', 'valueType': '', 'delayNum': ''}, {
                    'portColor': '#FFFFFF',
                    'portId': 'B',
                    'value': '',
                    'valueType': '',
                    'delayNum': ''
                }],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createMathActionDiv() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'DIV#' + rand,
                name: 'DIV',
                describtion: 'DIV',
                loc: '0 0',
                category: 'Math',
                value: '',
                valueType: '',
                delayNum: '',
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'A', 'value': '', 'valueType': '', 'delayNum': ''}, {
                    'portColor': '#FFFFFF',
                    'portId': 'B',
                    'value': '',
                    'valueType': '',
                    'delayNum': ''
                }],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );

    }

    createMathActionMultiple() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'Multiple#' + rand,
                name: 'Multiple',
                describtion: 'Multiple',
                loc: '0 0',
                category: 'Math',
                value: '',
                valueType: '',
                delayNum: '',
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'A', 'value': '', 'valueType': '', 'delayNum': ''}, {
                    'portColor': '#FFFFFF',
                    'portId': 'B',
                    'value': '',
                    'valueType': '',
                    'delayNum': ''
                }],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createMathActionCompare() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'Compare#' + rand,
                name: 'Compare',
                describtion: 'Compare',
                loc: '0 0',
                category: 'Math',
                value: '',
                valueType: '',
                delayNum: '',
                operation: '',
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'A', 'value': '', 'valueType': '', 'delayNum': ''}, {
                    'portColor': '#FFFFFF',
                    'portId': 'B',
                    'value': '',
                    'valueType': '',
                    'delayNum': ''
                }],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createExecActionCreateSwitch() {
        var rand = uuidv4();
        let bottomPort = new NodePort(
            '#316571',
            'eth0',
            'eth0',
            '',
            ''
        );
        let bottomArray: NodePort[] = [];
        bottomArray.push(bottomPort);
        this.myDiagram.model.addNodeData(
            {
                key: 'CreateSwitch#' + rand,
                name: 'CreateSwitch',
                describtion: 'CreateSwitch',
                loc: '0 0',
                category: 'Exec',
                value: '',
                valueType: '',
                delayNum: '',
                swch: new Switch(
                    '',
                    'Switch#' + rand,
                    'Switch',
                    'S#' + rand.split('-')[1],
                    '',
                    [],
                    [],
                    bottomArray,
                    []
                ),
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'left0', 'value': '', 'valueType': '', 'delayNum': ''}],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createExecActionCreateVNF() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'CreateVNF#' + rand,
                name: 'CreateVNF',
                describtion: 'CreateVNF',
                loc: '0 0',
                category: 'Exec',
                value: '',
                valueType: '',
                delayNum: '',
                vnf: new Vnf(
                    '',
                    'VNF#' + rand,
                    'VNF',
                    'V#' + rand.split('-')[1]
                ),
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'left0', 'value': '', 'valueType': '', 'delayNum': ''}],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createExecActionCreateSwitchPachPeer() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'CreatePatchPeer#' + rand,
                name: 'CreatePatchPeer',
                describtion: 'CreatePatchPeer',
                loc: '0 0',
                category: 'Exec',
                value: '',
                valueType: '',
                delayNum: '',
                nodeId: '',
                bridgeName1: '',
                bridgeName2: '',
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'left0', 'value': '', 'valueType': '', 'delayNum': ''}],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createExecActionCreateVNFPort() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'CreateVNFPort#' + rand,
                name: 'CreateVNFPort',
                describtion: 'CreateVNFPort',
                loc: '0 0',
                category: 'Exec',
                value: '',
                valueType: '',
                delayNum: '',
                nodeId: '',
                bridgeName: '',
                containerName: '',
                interfaceName: '',
                ipAddress: '',
                gateway: '',
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'left0', 'value': '', 'valueType': '', 'delayNum': ''}],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createExecActionCreateODL() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'CreateODL#' + rand,
                name: 'CreateODL',
                describtion: 'CreateODL',
                loc: '0 0',
                category: 'Exec',
                value: '',
                valueType: '',
                delayNum: '',
                nodeId: '',
                imgName: '',
                containerName: '',
                containerPortOpenFlowExpose: '',
                containerPortDluxExpose: '',
                containerPortCLIExpose: '',
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'left0', 'value': '', 'valueType': '', 'delayNum': ''}],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createExecActionUpdateVNF() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'UpdateVNF#' + rand,
                name: 'UpdateVNF',
                describtion: 'UpdateVNF',
                loc: '0 0',
                category: 'Exec',
                value: '',
                valueType: '',
                delayNum: '',
                nodeId: '',
                containerName: '',
                cpu: '',
                ram: '',
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'left0', 'value': '', 'valueType': '', 'delayNum': ''}],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createExecActionDeleteSwitch() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'DeleteSwitch#' + rand,
                name: 'DeleteSwitch',
                describtion: 'DeleteSwitch',
                loc: '0 0',
                category: 'Exec',
                value: '',
                valueType: '',
                delayNum: '',
                nodeId: '',
                bridgeName: '',
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'left0', 'value': '', 'valueType': '', 'delayNum': ''}],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createExecActionDeleteVNF() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'DeleteVNF#' + rand,
                name: 'DeleteVNF',
                describtion: 'DeleteVNF',
                loc: '0 0',
                category: 'Exec',
                value: '',
                valueType: '',
                delayNum: '',
                nodeId: '',
                vnfName: '',
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'left0', 'value': '', 'valueType': '', 'delayNum': ''}],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createExecActionDeleteSwitchPachPeer() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'DeletePatchPeer#' + rand,
                name: 'DeletePatchPeer',
                describtion: 'DeletePatchPeer',
                loc: '0 0',
                category: 'Exec',
                value: '',
                valueType: '',
                delayNum: '',
                nodeId: '',
                bridgeName1: '',
                bridgeName2: '',
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'left0', 'value': '', 'valueType': '', 'delayNum': ''}],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createExecActionDeleteVNFPort() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'DeleteVNFPort#' + rand,
                name: 'DeleteVNFPort',
                describtion: 'DeleteVNFPort',
                loc: '0 0',
                category: 'Exec',
                value: '',
                valueType: '',
                delayNum: '',
                nodeId: '',
                bridgeName: '',
                containerName: '',
                interfaceName: '',
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'left0', 'value': '', 'valueType': '', 'delayNum': ''}],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }

    createExecActionDeleteODL() {
        var rand = uuidv4();
        this.myDiagram.model.addNodeData(
            {
                key: 'DeleteODL#' + rand,
                name: 'DeleteODL',
                describtion: 'DeleteODL',
                loc: '0 0',
                category: 'Exec',
                value: '',
                valueType: '',
                delayNum: '',
                nodeId: '',
                odlName: '',
                leftArray: [{'portColor': '#FFFFFF', 'portId': 'left0', 'value': '', 'valueType': '', 'delayNum': ''}],
                topArray: [],
                bottomArray: [],
                rightArray: [{'portColor': '#FFFFFF', 'portId': 'right0', 'value': '', 'valueType': '', 'delayNum': ''}]
            }
        );
    }


    //Group Manipulation
    highlightGroup(e, grp, show) {
        if (!grp) return;
        e.handled = true;
        if (show) {
            // cannot depend on the grp.diagram.selection in the case of external drag-and-drops;
            // instead depend on the DraggingTool.draggedParts or .copiedParts
            var tool = grp.diagram.toolManager.draggingTool;
            var map = tool.draggedParts || tool.copiedParts;  // this is a Map
            // now we can check to see if the Group will accept membership of the dragged Parts
            if (grp.canAddMembers(map.toKeySet())) {
                grp.isHighlighted = true;
                return;
            }
        }
        grp.isHighlighted = false;
    }

    finishDrop(e, grp) {
        var ok = (grp !== null
            ? grp.addMembers(grp.diagram.selection, true)
            : e.diagram.commandHandler.addTopLevelParts(e.diagram.selection, true));
        console.log(ok);
        if (!ok) e.diagram.currentTool.doCancel();
    }


    makeButton(text, action, visiblePredicate ?) {
        return $$(
            'ContextMenuButton',
            $$(go.TextBlock, text),
            {click: action},
            // don't bother with binding GraphObject.visible if there's no predicate
            visiblePredicate
                ? new go.Binding('visible', '', function(o, e) {
                    return o.diagram ? visiblePredicate(o, e) : false;
                }).ofObject()
                : {}
        );
    }

    addPort(side) {
        console.log('here on add port');
        this.myDiagram.startTransaction('addPort');
        let __this = this.myDiagram;
        this.myDiagram.selection.each(function(node) {
            console.log(1);
            if (!(node instanceof go.Node)) return;
            var i = 0;
            while (node.findPort(side + i.toString()) !== node) i++;
            var name = side + i.toString();
            var arr = node.data[side + 'Array'];
            if (arr) {
                var newportdata = {
                    portId: name,
                    portColor: go.Brush.randomColor()
                };
                console.log(2);
                __this.model.insertArrayItem(arr, -1, newportdata);
            }
        });
        this.myDiagram = __this;
        this.myDiagram.commitTransaction('addPort');
    }

    removePort(port) {
        this.myDiagram.startTransaction('removePort');
        var pid = port.portId;
        var arr = port.panel.itemArray;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].portId === pid) {
                this.myDiagram.model.removeArrayItem(arr, i);
                break;
            }
        }
        this.myDiagram.commitTransaction('removePort');
    }

    removeAll(port) {
        this.myDiagram.startTransaction('removePorts');
        var nodedata = port.part.data;
        var side = port._side; // there are four property names, all ending in "Array"
        this.myDiagram.model.setDataProperty(nodedata, side + 'Array', []); // an empty Array
        this.myDiagram.commitTransaction('removePorts');
    }

    changeColor(port) {
        this.myDiagram.startTransaction('colorPort');
        var data = port.data;
        this.myDiagram.model.setDataProperty(
            data,
            'portColor',
            go.Brush.randomColor()
        );
        this.myDiagram.commitTransaction('colorPort');
    }


    load() {
        // this.myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
        this.myDiagram.model = go.Model.fromJson({
            class: 'go.GraphLinksModel',
            copiesArrays: true,
            copiesArrayObjects: true,
            linkFromPortIdProperty: 'fromPort',
            linkToPortIdProperty: 'toPort',
            nodeDataArray: [],
            linkDataArray: []
        });

        // this.server.networkCards.forEach((nic: NetworkCard, index) => {
        //     var rand = uuidv4();
        //     this.myDiagram.model.addNodeData(
        //         {
        //             source: this.sampleMainServerInterface,
        //             key: 'MainServerInterface#' + rand,
        //             name: 'MainServerInterface',
        //             describtion: nic.name + '#' + nic.ipAddress,
        //             loc: new go.Point(0, 0 + (index * 160)),
        //             serverId: '',
        //             category: 'Nic',
        //             leftArray: [],
        //             topArray: [],
        //             bottomArray: [],
        //             rightArray: []
        //         }
        //     );
        // });
    }

    zoomIn() {
        this.myDiagram.commandHandler.increaseZoom()
    }


    zoomOut() {
        this.myDiagram.commandHandler.decreaseZoom()
    }

    fitToView() {
        this.myDiagram.zoomToRect(this.myDiagram.documentBounds)
    }

    undo() {
        this.myDiagram.commandHandler.undo()
    }

    redo() {
        this.myDiagram.commandHandler.redo()
    }

    clearDiagram() {
        this.myDiagram.clear();
    }

    saveDiagram() {
        if (this.eventObj.name === '' || this.eventObj.name === null || this.eventObj.name === undefined) {
            this.notificationService.smallBox({
                title: 'Event name is empty',
                content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
                color: '#a00002',
                iconSmall: 'fa fa-times  bounce animated',
                timeout: 8000
            });
        }
        else {
            this.tabSpinnerText = 'Create New Event';
            this.ng4LoadingSpinnerService.show();
            this.eventObj.eventDiagram = new EventDiagramParserUtil().assignPriorityNumToNodes(new EventDiagramParserUtil().convertDiagramJsonToTopology(this.myDiagram.model.toJSON()));
            this.eventObj.numberOfOccur = '0';
            this.eventObj.status = 'down';
            this.eventObj.eventJson = this.myDiagram.model.toJSON();
            this.eventService.save('/new', this.eventObj).subscribe((res: any) => {
                this.onSaveSuccess();
            }, (err) => {
                this.ng4LoadingSpinnerService.hide();
                this.tabSpinnerText = '';
                this.notificationService.smallBox({
                    title: err ? err : 'Undefined Error',
                    content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
                    color: '#a00002',
                    iconSmall: 'fa fa-times  bounce animated',
                    timeout: 8000
                });
            });
        }
    }

    private onSaveSuccess() {
        this.ng4LoadingSpinnerService.hide();
        this.tabSpinnerText = '';
        this.router.navigate(['/events']);
    }

    backToPrevPage() {
        this.router.navigate(['/events']);
    }

    saveDiagramOnLocal() {
        var blob = new Blob([this.myDiagram.model.toJson()], {type: 'text/plain'});
        FileSaver.saveAs(blob, 'XenonetEvent' + '.xnetevt');
    }

    loadDiagram(event) {
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let formData: FormData = new FormData();
            formData.append('uploadFile', file, file.name);
            let reader = new FileReader();
            reader.onloadend = (e) => {
                this.clearDiagram();
                this.myDiagram.model = go.Model.fromJson(reader.result);
            };
            reader.readAsText(file);
        }
    }

    /*
    Apply Change to entities
     */
    applyChangeToCreateExternalNotifier(extProp: NotifierExternalProperties) {
        if (this.selectedPart) {
            this.myDiagram.startTransaction('set all properties');
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'valueType',
                extProp.valueType
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'topicName',
                extProp.topicName
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'isLatch',
                extProp.isLatch ? '1' : '0'
            );
            this.myDiagram.commitTransaction('set all properties');
        }
        this.notificationService.smallBox({
            title: 'Applied successfully',
            content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
            color: '#3276b1',
            iconSmall: 'fa fa-check bounce animated',
            timeout: 2000
        });
    }

    applyChangeToCreateVnfNotifier(notifierVnfProperties: NotifierVnfProperties) {
        if (this.selectedPart) {
            this.myDiagram.startTransaction('set all properties');
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'topicName',
                notifierVnfProperties.topicName
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'containerName',
                notifierVnfProperties.containerName
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'metric',
                notifierVnfProperties.metric
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'interfaceName',
                notifierVnfProperties.interfaceName
            );
            this.myDiagram.commitTransaction('set all properties');
        }
        this.notificationService.smallBox({
            title: 'Applied successfully',
            content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
            color: '#3276b1',
            iconSmall: 'fa fa-check bounce animated',
            timeout: 2000
        });
    }

    applyChangeToMathCompare(mathCompareProperties: MathCompareProperties) {
        if (this.selectedPart) {
            this.myDiagram.startTransaction('set all properties');
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'operation',
                mathCompareProperties.operation
            );
            this.myDiagram.commitTransaction('set all properties');
        }
        this.notificationService.smallBox({
            title: 'Applied successfully',
            content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
            color: '#3276b1',
            iconSmall: 'fa fa-check bounce animated',
            timeout: 2000
        });
    }

    applyChangeToCreateSwitch(swch) {
        if (this.selectedPart) {
            this.myDiagram.startTransaction('set all properties');
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'swch',
                swch
            );
            this.myDiagram.commitTransaction('set all properties');
        }
        this.notificationService.smallBox({
            title: 'Applied successfully',
            content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
            color: '#3276b1',
            iconSmall: 'fa fa-check bounce animated',
            timeout: 2000
        });
    }

    applyChangeToCreateVnf(vnf) {
        if (this.selectedPart) {
            this.myDiagram.startTransaction('set all properties');
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'vnf',
                vnf
            );
            this.myDiagram.commitTransaction('set all properties');
        }
        this.notificationService.smallBox({
            title: 'Applied successfully',
            content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
            color: '#3276b1',
            iconSmall: 'fa fa-check bounce animated',
            timeout: 2000
        });
    }

    applyChangeToCreateBridgePatchPeerPort(createBridgePatchPeerPort: CreateBridgePatchPeerPortProperties) {
        if (this.selectedPart) {
            this.myDiagram.startTransaction('set all properties');
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'nodeId',
                createBridgePatchPeerPort.serverId
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'bridgeName1',
                createBridgePatchPeerPort.bridgeName1
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'bridgeName2',
                createBridgePatchPeerPort.bridgeName2
            );
            this.myDiagram.commitTransaction('set all properties');
        }
        this.notificationService.smallBox({
            title: 'Applied successfully',
            content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
            color: '#3276b1',
            iconSmall: 'fa fa-check bounce animated',
            timeout: 2000
        });
    }

    applyChangeToCreateVnfPort(createVnfPortProperties: CreateVnfPortProperties) {
        if (this.selectedPart) {
            this.myDiagram.startTransaction('set all properties');
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'nodeId',
                createVnfPortProperties.serverId
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'bridgeName',
                createVnfPortProperties.bridgeName
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'containerName',
                createVnfPortProperties.containerName
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'interfaceName',
                createVnfPortProperties.interfaceName
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'ipAddress',
                createVnfPortProperties.ipAddress
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'gateway',
                createVnfPortProperties.gateway
            );
            this.myDiagram.commitTransaction('set all properties');
        }
        this.notificationService.smallBox({
            title: 'Applied successfully',
            content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
            color: '#3276b1',
            iconSmall: 'fa fa-check bounce animated',
            timeout: 2000
        });
    }

    applyChangeToUpdateVnf(updateVnfProperties: UpdateVnfProperties) {
        if (this.selectedPart) {
            this.myDiagram.startTransaction('set all properties');
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'nodeId',
                updateVnfProperties.serverId
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'containerName',
                updateVnfProperties.containerName
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'cpu',
                updateVnfProperties.cpu
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'ram',
                updateVnfProperties.ram
            );
            this.myDiagram.commitTransaction('set all properties');
        }
        this.notificationService.smallBox({
            title: 'Applied successfully',
            content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
            color: '#3276b1',
            iconSmall: 'fa fa-check bounce animated',
            timeout: 2000
        });
    }

    applyChangeToMathStatic(mathStaticProperties: MathStaticProperties) {
        if (this.selectedPart) {
            this.myDiagram.startTransaction('set all properties');
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'value',
                mathStaticProperties.value
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'valueType',
                mathStaticProperties.valueType
            );
            this.myDiagram.commitTransaction('set all properties');
        }
        this.notificationService.smallBox({
            title: 'Applied successfully',
            content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
            color: '#3276b1',
            iconSmall: 'fa fa-check bounce animated',
            timeout: 2000
        });
    }

    applyChangeToDeleteSwitch(deleteBridgeProperties: DeleteBridgeProperties) {
        if (this.selectedPart) {
            this.myDiagram.startTransaction('set all properties');
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'nodeId',
                deleteBridgeProperties.serverId
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'bridgeName',
                deleteBridgeProperties.bridgeName
            );
            this.myDiagram.commitTransaction('set all properties');
        }
        this.notificationService.smallBox({
            title: 'Applied successfully',
            content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
            color: '#3276b1',
            iconSmall: 'fa fa-check bounce animated',
            timeout: 2000
        });
    }

    applyChangeToDeleteVnf(deleteVnfProperties: DeleteVnfProperties) {
        if (this.selectedPart) {
            this.myDiagram.startTransaction('set all properties');
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'nodeId',
                deleteVnfProperties.serverId
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'vnfName',
                deleteVnfProperties.vnfName
            );
            this.myDiagram.commitTransaction('set all properties');
        }
        this.notificationService.smallBox({
            title: 'Applied successfully',
            content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
            color: '#3276b1',
            iconSmall: 'fa fa-check bounce animated',
            timeout: 2000
        });
    }

    applyChangeToDeleteBridgePatchPeerPort(deleteBridgePatchPeerPortProperties: DeleteBridgePatchPeerPortProperties) {
        if (this.selectedPart) {
            this.myDiagram.startTransaction('set all properties');
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'nodeId',
                deleteBridgePatchPeerPortProperties.serverId
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'bridgeName1',
                deleteBridgePatchPeerPortProperties.bridgeName1
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'bridgeName2',
                deleteBridgePatchPeerPortProperties.bridgeName2
            );
            this.myDiagram.commitTransaction('set all properties');
        }
        this.notificationService.smallBox({
            title: 'Applied successfully',
            content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
            color: '#3276b1',
            iconSmall: 'fa fa-check bounce animated',
            timeout: 2000
        });
    }

    applyChangeToDeleVnfPort(deleteVnfPortProperties: DeleteVnfPortProperties) {
        if (this.selectedPart) {
            this.myDiagram.startTransaction('set all properties');
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'nodeId',
                deleteVnfPortProperties.serverId
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'containerName',
                deleteVnfPortProperties.containerName
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'bridgeName',
                deleteVnfPortProperties.bridgeName
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'interfaceName',
                deleteVnfPortProperties.interfaceName
            );
            this.myDiagram.commitTransaction('set all properties');
        }
        this.notificationService.smallBox({
            title: 'Applied successfully',
            content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
            color: '#3276b1',
            iconSmall: 'fa fa-check bounce animated',
            timeout: 2000
        });
    }

}
