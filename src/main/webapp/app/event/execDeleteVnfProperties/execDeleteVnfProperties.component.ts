import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Server} from '../../servers/server.model';
import {DeleteVnfProperties} from './deleteVnfProperties.model';

const uuidv4 = require('uuid/v4');
@Component({
    selector: "Xnet-execDeleteVnfPropertiesComponent",
    styles: [],
    templateUrl: "./execDeleteVnfProperties.component.html"
})
export class ExecDeleteVnfPropertiesComponent implements OnInit{
    @Input() deleteVnfProperties: DeleteVnfProperties;
    @Input() servers: Server[];
    @Output() applyChange = new EventEmitter();

    constructor() {}

    ngOnInit(){

    }

    emitApplyChange() {
        this.applyChange.emit(this.deleteVnfProperties);
    }
}
