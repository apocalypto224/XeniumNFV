import {EventDiagram} from './eventDiagram.model';
import {NotifierSwitch} from './eventNodes/notifier/notifierSwitch.model';
import {NotifierVNF} from './eventNodes/notifier/notifierVNF.model';
import {NotifierExternal} from './eventNodes/notifier/notifierExternal.model';
import {LogicalAnd} from './eventNodes/logic/logicalAnd.model';
import {LogicalOr} from './eventNodes/logic/logicalOr.model';
import {MathStaticVal} from './eventNodes/math/mathStaticVal.model';
import {MathAdd} from './eventNodes/math/mathAdd.model';
import {MathSub} from './eventNodes/math/mathSub.model';
import {MathDiv} from './eventNodes/math/mathDiv.model';
import {MathMultiple} from './eventNodes/math/mathMultiple.model';
import {MathCompare} from './eventNodes/math/mathCompare.model';
import {CreateSwitch} from './eventNodes/exec/createSwitch.model';
import {CreateVnf} from './eventNodes/exec/createVnf.model';
import {CreateSwitchPatchPeer} from './eventNodes/exec/createSwitchPatchPeer.model';
import {CreateVnfPort} from './eventNodes/exec/createVnfPort.model';
import {CreateOdl} from './eventNodes/exec/createOdl.model';
import {UpdateVnf} from './eventNodes/exec/updateVnf.model';
import {DeleteSwitch} from './eventNodes/exec/deleteSwitch.model';
import {DeleteVnf} from './eventNodes/exec/deleteVnf.model';
import {DeleteSwitchPatchPeer} from './eventNodes/exec/deleteSwitchPatchPeer.model';
import {DeleteVnfPort} from './eventNodes/exec/deleteVnfPort.model';
import {DeleteOdl} from './eventNodes/exec/deleteOdl.model';
import {EventDiagramEntityConnection} from './eventNodes/eventDiagramEntityConnection.model';

const uuidv4 = require('uuid/v4');

export class EventDiagramParserUtil {

    assignPriorityNumToNodes(eventDiagram: EventDiagram) {
        let priorityCounter = 0;

        /*
        Set all notifiers priorityNum to 0
         */
        eventDiagram.notifierExternal.forEach(notifierExternal => {
            notifierExternal.priorityNum = '0';
        });
        eventDiagram.notifierSwitch.forEach(notifierSwitch => {
            notifierSwitch.priorityNum = '0';
        });
        eventDiagram.notifierVnf.forEach(notifierVnf => {
            notifierVnf.priorityNum = '0';
        });
        eventDiagram.mathStaticVal.forEach(mathStaticVal => {
            mathStaticVal.priorityNum = '0';
        });
        /*
        "eventDiagramEntityConnection" key is like -> FromKey%ToKey ->
         "NotifierSwitch#3c702828-4b43-44e0-8393-70f04f7fea88%AND#a0839f57-81be-4ef6-b4e7-eabb568e9987"
         so in order to find the output of each notifier we are going to loop into the "eventDiagramEntityConnection" and split its FROM part
         */
        eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
            if (eventDiagramEntityConnection.key.split('%')[0].split('#')[0] === 'NotifierSwitch' ||
                eventDiagramEntityConnection.key.split('%')[0].split('#')[0] === 'NotifierVnf' ||
                eventDiagramEntityConnection.key.split('%')[0].split('#')[0] === 'NotifierExt' ||
                eventDiagramEntityConnection.key.split('%')[0].split('#')[0] === 'StaticVal') {
                eventDiagramEntityConnection.priorityNum = '0';
            }
        });
        /*
        BANNING EVENT NODE
        Assume this diagram
                                   ------> |---------------|
                                          |      And      |----->|---------------|
                                 ------> |---------------|      |      OR       |--------->
                                                       ------> |---------------|
         On array list of logical we have AND on index 0 and OR on index 1 so when we parsing array we meet the and first so far so good
         we assign the Priority-Num to AND and its port and then we meet the OR block on same iteration so we assign the same Priority-Num to it
         So here is the bug we want the AND block has Priority-Num 1 and the OR has 2;
         In order to solve it we create the Array Called "bannedNode" that we save the next block of each particular node in it on each iteration. So that
         when we reach to it on the save iteration we ignore it. Also we Clear it on each loop of our "while(true)" loop; Also "bannedNode" is shared on each loop
         of block coz on above diagram maybe we connect the AND to The Compare ( logic block to the Math block !!!)
         */
        while (true) {
            priorityCounter++;
            let bannedNode: string[] = [];
            let isAllNodeHaveTag = [];
            console.log("----------------------------------------------------------------------------");
            console.log(priorityCounter);
            console.log(bannedNode);
            console.log(isAllNodeHaveTag);
            console.log("---------------------");

            // Logic AND Part
            //********************************************************************************************************************************************
            eventDiagram.logicAnd.forEach(logicAnd => {
                if (logicAnd.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(logicAnd.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (logicAnd.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            logicAnd.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (logicAnd.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });
            // Logic OR Part
            //********************************************************************************************************************************************
            eventDiagram.logicOr.forEach(logicOr => {
                if (logicOr.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(logicOr.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (logicOr.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            logicOr.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (logicOr.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });
            // Math ADD Part
            //********************************************************************************************************************************************
            eventDiagram.mathAdd.forEach(mathAdd => {
                if (mathAdd.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(mathAdd.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (mathAdd.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            mathAdd.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (mathAdd.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });
            // Math MathCompare Part
            //********************************************************************************************************************************************
            eventDiagram.mathCompare.forEach(mathCompare => {
                if (mathCompare.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(mathCompare.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (mathCompare.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            mathCompare.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (mathCompare.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });
            // Math MathDiv Part
            //********************************************************************************************************************************************
            eventDiagram.mathDiv.forEach(mathDiv => {
                if (mathDiv.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(mathDiv.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (mathDiv.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            mathDiv.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (mathDiv.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });
            // Math MathMultiple Part
            //********************************************************************************************************************************************
            eventDiagram.mathMultiple.forEach(mathMultiple => {
                if (mathMultiple.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(mathMultiple.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (mathMultiple.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            mathMultiple.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (mathMultiple.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });
            // Math MathSub Part
            //********************************************************************************************************************************************
            eventDiagram.mathSub.forEach(mathSub => {
                if (mathSub.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(mathSub.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (mathSub.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            mathSub.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (mathSub.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });
            // EXEC CreateOdl  Part
            //********************************************************************************************************************************************
            eventDiagram.createOdl.forEach(createOdl => {
                if (createOdl.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(createOdl.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (createOdl.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            createOdl.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (createOdl.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });
            // EXEC CreateSwitch  Part
            //********************************************************************************************************************************************
            eventDiagram.createSwitch.forEach(createSwitch => {
                if (createSwitch.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(createSwitch.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (createSwitch.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            createSwitch.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (createSwitch.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });
            // EXEC CreateSwitchPatchPeer  Part
            //********************************************************************************************************************************************
            eventDiagram.createSwitchPatchPeer.forEach(createSwitchPatchPeer => {
                if (createSwitchPatchPeer.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(createSwitchPatchPeer.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (createSwitchPatchPeer.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            createSwitchPatchPeer.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (createSwitchPatchPeer.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });
            // EXEC CreateVnf  Part
            //********************************************************************************************************************************************
            eventDiagram.createVnf.forEach(createVnf => {
                console.log('create vmf');
                if (createVnf.priorityNum === '') {
                    console.log('priorityNum === null');
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(createVnf.key) < 0) {
                        console.log('no bann');
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (createVnf.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            createVnf.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (createVnf.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                    console.log('priorityNum !== null');
                }
            });
            // EXEC CreateVnfPort  Part
            //********************************************************************************************************************************************
            eventDiagram.createVnfPort.forEach(createVnfPort => {
                if (createVnfPort.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(createVnfPort.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (createVnfPort.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            createVnfPort.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (createVnfPort.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });
            // EXEC DeleteOdl  Part
            //********************************************************************************************************************************************
            eventDiagram.deleteOdl.forEach(deleteOdl => {
                if (deleteOdl.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(deleteOdl.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (deleteOdl.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            deleteOdl.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (deleteOdl.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });
            // EXEC DeleteSwitch  Part
            //********************************************************************************************************************************************
            eventDiagram.deleteSwitch.forEach(deleteSwitch => {
                if (deleteSwitch.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(deleteSwitch.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (deleteSwitch.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            deleteSwitch.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (deleteSwitch.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });
            // EXEC DeleteSwitchPatchPeer  Part
            //********************************************************************************************************************************************
            eventDiagram.deleteSwitchPatchPeer.forEach(deleteSwitchPatchPeer => {
                if (deleteSwitchPatchPeer.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(deleteSwitchPatchPeer.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (deleteSwitchPatchPeer.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            deleteSwitchPatchPeer.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (deleteSwitchPatchPeer.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });
            // EXEC DeleteVnf  Part
            //********************************************************************************************************************************************
            eventDiagram.deleteVnf.forEach(deleteVnf => {
                if (deleteVnf.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(deleteVnf.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (deleteVnf.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            deleteVnf.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (deleteVnf.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });
            // EXEC DeleteVnfPort  Part
            //********************************************************************************************************************************************
            eventDiagram.deleteVnfPort.forEach(deleteVnfPort => {
                if (deleteVnfPort.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(deleteVnfPort.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (deleteVnfPort.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            deleteVnfPort.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (deleteVnfPort.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });
            // EXEC UpdateVnf  Part
            //********************************************************************************************************************************************
            eventDiagram.updateVnf.forEach(updateVnf => {
                if (updateVnf.priorityNum === '') {
                    isAllNodeHaveTag.push('0');
                    if (bannedNode.indexOf(updateVnf.key) < 0) {
                        let isInputPortsAvailable = true;
                        for (let eventDiagramEntityConnection of eventDiagram.eventDiagramEntityConnection) {
                            if (updateVnf.key === eventDiagramEntityConnection.key.split('%')[1]) {
                                if (eventDiagramEntityConnection.priorityNum === '') {
                                    isInputPortsAvailable = false;
                                    break;
                                }
                            }
                        }
                        if (isInputPortsAvailable) {
                            updateVnf.priorityNum = priorityCounter.toString();
                            /*
                            Assign priority to output ports
                             */
                            eventDiagram.eventDiagramEntityConnection.forEach(eventDiagramEntityConnection => {
                                if (updateVnf.key === eventDiagramEntityConnection.key.split('%')[0]) {
                                    eventDiagramEntityConnection.priorityNum = priorityCounter.toString();
                                    //Banning the next block of this node
                                    bannedNode.push(eventDiagramEntityConnection.key.split('%')[1])
                                }
                            });
                        }
                    }
                }
                else {
                    isAllNodeHaveTag.push('1');
                }
            });

            console.log(priorityCounter);
            console.log(bannedNode);
            console.log(isAllNodeHaveTag);

            if (isAllNodeHaveTag.indexOf('0') < 0 || priorityCounter > 500) {
                /*
                This is for
                ----------------------------------------------------------------------------
                8 --> number of iteration
                Array []
                Array []
                ---------------------
                create vmf
                priorityNum !== null
                8--> number of iteration
                Array []
                Array [ "1", "1", "1", "1", "1", "1", "1", "1", "0" ] ==> isAllNodeHaveTag
                ----------------------------------------------------------------------------
                9--> number of iteration
                Array []
                Array []
                ---------------------
                create vmf
                priorityNum !== null
                9--> number of iteration
                Array []
                Array [ "1", "1", "1", "1", "1", "1", "1", "1", "1" ] ==> isAllNodeHaveTag


                We already done on iteration 8 (for last node at first no priority has been assigned on the iteration 8 we push '0' to the "isAllNodeHaveTag" array
                that's why on iteration 8 we have "Array [ "1", "1", "1", "1", "1", "1", "1", "1", "0" ] " but after we pushed the '0' we assign the priority on the next if to
                that node "if (bannedNode.indexOf(updateVnf.key) < 0) {" so the last node get the priority but at the end of the while(true) loop we check the "isAllNodeHaveTag"
                and its obvious that it failed and going for another loop and so  "Array [ "1", "1", "1", "1", "1", "1", "1", "1", "1" ]" created and the break the infinite loop
                work. so in all diagram we loop extra one (we have to) so we can use "priorityCounter -1" to get exact maxPriorityNum.
                 */
                eventDiagram.maxPriorityNum = (priorityCounter -1).toString();
                // break the infinite while loop
                break;
            }
        }
        console.log(eventDiagram);
        return eventDiagram;
    }

    convertDiagramJsonToTopology(jSonStr: any) {
        let eventDiagram: EventDiagram = new EventDiagram();
        JSON.parse(jSonStr).nodeDataArray.forEach(element => {
            if (element.name === 'NotifierSwitch') {
                eventDiagram.notifierSwitch.push(new NotifierSwitch(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    new Array()
                    )
                )
            }
            else if (element.name === 'NotifierVnf') {
                eventDiagram.notifierVnf.push(new NotifierVNF(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.topicName,
                    element.containerName,
                    element.metric,
                    element.interfaceName,
                    new Array()
                    )
                )
            }
            else if (element.name === 'NotifierExt') {
                eventDiagram.notifierExternal.push(new NotifierExternal(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.topicName,
                    element.valueType,
                    element.isLatch,
                    new Array()
                    )
                )
            }
            else if (element.name === 'AND') {
                eventDiagram.logicAnd.push(new LogicalAnd(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'BOOLEAN',
                    new Array()
                    )
                )
            }
            else if (element.name === 'OR') {
                eventDiagram.logicOr.push(new LogicalOr(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'BOOLEAN',
                    new Array()
                    )
                )
            }
            else if (element.name === 'StaticVal') {
                eventDiagram.mathStaticVal.push(new MathStaticVal(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    element.valueType,
                    new Array()
                    )
                )
            }
            else if (element.name === 'ADD') {
                eventDiagram.mathAdd.push(new MathAdd(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'INTEGER',
                    new Array()
                    )
                )
            }
            else if (element.name === 'SUB') {
                eventDiagram.mathSub.push(new MathSub(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'INTEGER',
                    new Array()
                    )
                )
            }
            else if (element.name === 'DIV') {
                eventDiagram.mathDiv.push(new MathDiv(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'INTEGER',
                    new Array()
                    )
                )
            }
            else if (element.name === 'Multiple') {
                eventDiagram.mathMultiple.push(new MathMultiple(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'INTEGER',
                    new Array()
                    )
                )
            }
            else if (element.name === 'Compare') {
                eventDiagram.mathCompare.push(new MathCompare(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'BOOLEAN',
                    element.operation,
                    new Array()
                    )
                )
            }
            else if (element.name === 'CreateSwitch') {
                eventDiagram.createSwitch.push(new CreateSwitch(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'BOOLEAN',
                    element.swch,
                    new Array()
                    )
                )
            }
            else if (element.name === 'CreateVNF') {
                eventDiagram.createVnf.push(new CreateVnf(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'BOOLEAN',
                    element.vnf,
                    new Array()
                    )
                )
            }
            else if (element.name === 'CreatePatchPeer') {
                eventDiagram.createSwitchPatchPeer.push(new CreateSwitchPatchPeer(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'BOOLEAN',
                    element.nodeId,
                    element.bridgeName1,
                    element.bridgeName2,
                    new Array()
                    )
                )
            }
            else if (element.name === 'CreateVNFPort') {
                eventDiagram.createVnfPort.push(new CreateVnfPort(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'BOOLEAN',
                    element.nodeId,
                    element.bridgeName,
                    element.containerName,
                    element.interfaceName,
                    element.ipAddress,
                    element.gateway,
                    new Array()
                    )
                )
            }
            else if (element.name === 'CreateODL') {
                eventDiagram.createOdl.push(new CreateOdl(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'BOOLEAN',
                    element.nodeId,
                    element.imgName,
                    element.containerName,
                    element.containerPortOpenFlowExpose,
                    element.containerPortDluxExpose,
                    element.containerPortCLIExpose,
                    new Array()
                    )
                )
            }
            else if (element.name === 'UpdateVNF') {
                eventDiagram.updateVnf.push(new UpdateVnf(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'BOOLEAN',
                    element.nodeId,
                    element.containerName,
                    element.cpu,
                    element.ram,
                    new Array()
                    )
                )
            }
            else if (element.name === 'DeleteSwitch') {
                eventDiagram.deleteSwitch.push(new DeleteSwitch(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'BOOLEAN',
                    element.nodeId,
                    element.bridgeName,
                    new Array()
                    )
                )
            }
            else if (element.name === 'DeleteVNF') {
                eventDiagram.deleteVnf.push(new DeleteVnf(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'BOOLEAN',
                    element.nodeId,
                    element.vnfName,
                    new Array()
                    )
                )
            }
            else if (element.name === 'DeletePatchPeer') {
                eventDiagram.deleteSwitchPatchPeer.push(new DeleteSwitchPatchPeer(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'BOOLEAN',
                    element.nodeId,
                    element.bridgeName1,
                    element.bridgeName2,
                    new Array()
                    )
                )
            }
            else if (element.name === 'DeleteVNFPort') {
                eventDiagram.deleteVnfPort.push(new DeleteVnfPort(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'BOOLEAN',
                    element.nodeId,
                    element.bridgeName,
                    element.containerName,
                    element.interfaceName,
                    new Array()
                    )
                )
            }
            else if (element.name === 'DeleteODL') {
                eventDiagram.deleteOdl.push(new DeleteOdl(
                    element.key,
                    element.name,
                    element.describtion,
                    element.loc,
                    [],
                    [],
                    [],
                    [],
                    element.group,
                    element.category,
                    element.priorityNum,
                    element.value,
                    'BOOLEAN',
                    element.nodeId,
                    element.odlName,
                    new Array()
                    )
                )
            }
        });
        JSON.parse(jSonStr).linkDataArray.forEach(element => {
            // CHECK FROM 'S
            if (element.from.split('#')[0] === 'NotifierSwitch') {
                eventDiagram.notifierSwitch.forEach(notifierSwitch => {
                    if (notifierSwitch.key === element.from) {
                        notifierSwitch.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'NotifierVnf') {
                eventDiagram.notifierVnf.forEach(notifierVnf => {
                    if (notifierVnf.key === element.from) {
                        notifierVnf.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'NotifierExt') {
                eventDiagram.notifierExternal.forEach(notifierExternal => {
                    if (notifierExternal.key === element.from) {
                        notifierExternal.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'AND') {
                eventDiagram.logicAnd.forEach(logicAnd => {
                    if (logicAnd.key === element.from) {
                        logicAnd.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'OR') {
                eventDiagram.logicOr.forEach(logicOr => {
                    if (logicOr.key === element.from) {
                        logicOr.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'StaticVal') {
                eventDiagram.mathStaticVal.forEach(mathStaticVal => {
                    if (mathStaticVal.key === element.from) {
                        mathStaticVal.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'ADD') {
                eventDiagram.mathAdd.forEach(mathAdd => {
                    if (mathAdd.key === element.from) {
                        mathAdd.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'SUB') {
                eventDiagram.mathSub.forEach(mathSub => {
                    if (mathSub.key === element.from) {
                        mathSub.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'DIV') {
                eventDiagram.mathDiv.forEach(mathDiv => {
                    if (mathDiv.key === element.from) {
                        mathDiv.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'Multiple') {
                eventDiagram.mathMultiple.forEach(mathMultiple => {
                    if (mathMultiple.key === element.from) {
                        mathMultiple.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'Compare') {
                eventDiagram.mathCompare.forEach(mathCompare => {
                    if (mathCompare.key === element.from) {
                        mathCompare.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'CreateSwitch') {
                eventDiagram.createSwitch.forEach(createSwitch => {
                    if (createSwitch.key === element.from) {
                        createSwitch.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'CreateVNF') {
                eventDiagram.createVnf.forEach(createVnf => {
                    if (createVnf.key === element.from) {
                        createVnf.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'CreatePatchPeer') {
                eventDiagram.createSwitchPatchPeer.forEach(createSwitchPatchPeer => {
                    if (createSwitchPatchPeer.key === element.from) {
                        createSwitchPatchPeer.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'CreateVNFPort') {
                eventDiagram.createVnfPort.forEach(createVnfPort => {
                    if (createVnfPort.key === element.from) {
                        createVnfPort.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'CreateODL') {
                eventDiagram.createOdl.forEach(createOdl => {
                    if (createOdl.key === element.from) {
                        createOdl.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'UpdateVNF') {
                eventDiagram.updateVnf.forEach(updateVnf => {
                    if (updateVnf.key === element.from) {
                        updateVnf.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'DeleteSwitch') {
                eventDiagram.deleteSwitch.forEach(deleteSwitch => {
                    if (deleteSwitch.key === element.from) {
                        deleteSwitch.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'DeleteVNF') {
                eventDiagram.deleteVnf.forEach(deleteVnf => {
                    if (deleteVnf.key === element.from) {
                        deleteVnf.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'DeletePatchPeer') {
                eventDiagram.deleteSwitchPatchPeer.forEach(deleteSwitchPatchPeer => {
                    if (deleteSwitchPatchPeer.key === element.from) {
                        deleteSwitchPatchPeer.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'DeleteVNFPort') {
                eventDiagram.deleteVnfPort.forEach(deleteVnfPort => {
                    if (deleteVnfPort.key === element.from) {
                        deleteVnfPort.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'DeleteODL') {
                eventDiagram.deleteOdl.forEach(deleteOdl => {
                    if (deleteOdl.key === element.from) {
                        deleteOdl.eventDiagramEntityConnections.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        eventDiagram.eventDiagramEntityConnection.push(new EventDiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            '',
                            '',
                            '',
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }

            // CHECK TOOOO 'S
            // if (element.to.split('#')[0] === 'VNF') {
            //     topology.vnfs.forEach(vnf => {
            //         if (vnf.key === element.to) {
            //             vnf.diagramEntityConnections.push(new DiagramEntityConnection(
            //                 -1,
            //                 element.from + '%' + element.to,
            //                 element.from,
            //                 element.to,
            //                 element.fromPort,
            //                 element.toPort
            //             ))
            //         }
            //     })
            // }
            // else if (element.to.split('#')[0] === 'Switch') {
            //     topology.switches.forEach(swtch => {
            //         if (swtch.key === element.to) {
            //             swtch.diagramEntityConnections.push(new DiagramEntityConnection(
            //                 -1,
            //                 element.from + '%' + element.to,
            //                 element.from,
            //                 element.to,
            //                 element.fromPort,
            //                 element.toPort
            //             ))
            //         }
            //     })
            // }
            // else if (element.to.split('#')[0] === 'Controller') {
            //     topology.controllers.forEach(ctr => {
            //         if (ctr.key === element.to) {
            //             ctr.diagramEntityConnections.push(new DiagramEntityConnection(
            //                 -1,
            //                 element.from + '%' + element.to,
            //                 element.from,
            //                 element.to,
            //                 element.fromPort,
            //                 element.toPort
            //             ))
            //         }
            //     })
            // }
            // else if (element.to.split('#')[0] === 'MainServerInterface') {
            //     topology.mainServerInterface.forEach(msi => {
            //         if (msi.key === element.to) {
            //             msi.diagramEntityConnections.push(new DiagramEntityConnection(
            //                 -1,
            //                 element.from + '%' + element.to,
            //                 element.from,
            //                 element.to,
            //                 element.fromPort,
            //                 element.toPort
            //             ))
            //         }
            //     })
            // }


        });
        console.log(eventDiagram);
        return eventDiagram;
    }

}
