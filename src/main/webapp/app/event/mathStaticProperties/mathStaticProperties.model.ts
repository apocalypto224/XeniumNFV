export class MathStaticProperties {
    constructor(public value?: string,
                public valueType?: string) {
        this.value = value ? value : '';
        this.valueType = valueType ? valueType : '';
    }
}
